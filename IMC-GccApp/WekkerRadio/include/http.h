#ifndef _Http_H
#define _Http_H
/*
 *  Copyright STREAMIT BV, 2010.
 *
 *  Project             : SIR
 *  Module              : Http
 *  File name  $Workfile: Http.h  $
 *       Last Save $Date: 2003/08/23 19:06:50  $
 *             $Revision: 0.1  $
 *  Creation Date       : 2003/08/23 19:06:50
 *
 *  Description         : Http client routines
 *
 */

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>

/*--------------------------------------------------------------------------*/
/*  Constant definitions                                                    */
/*--------------------------------------------------------------------------*/
/*!\brief Influence the behaviour of HttpSendRequest */
#define HTTP_AUTH   0x02

/*--------------------------------------------------------------------------*/
/*  Type declarations                                                       */
/*--------------------------------------------------------------------------*/
/*!\brief Url indexer */
typedef struct _TUrlParts
{
    char *pszHost;
    char *pszPort;
    char *pszPath;
} TUrlParts;

/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  Global functions                                                        */
/*--------------------------------------------------------------------------*/
extern int      Base64EncodedSize(size_t tNrOfBytes);
extern size_t   Base64Encode(char *szDest, CONST u_char *pSrc, size_t tSize);

extern u_long   GetHostByName(CONST char *szHostName);

extern void     HttpParseUrl(char *szUrl, TUrlParts *tUrlParts);

extern int      HttpSendRequest(FILE *ptStream, CONST char *pszHeaders, u_short wMode);

#endif /* _Http_H */
