
#include "typedefs.h"

/*-------------------------------------------------------------------------*/
/* global defines                                                          */
/*-------------------------------------------------------------------------*/
#define CARD_IS_NOT_PRESENT           0
#define CARD_IS_PRESENT               1
#define CARD_NO_CHANGE                2       // no change since last event occured

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/

extern void CardInit(void);
extern int CardInitCard( void );
//extern TError CardOpen(u_char);
extern void CardClose(void);
extern TError CardStatus(void);
extern unsigned int CardGetCurrentSong(void);
extern char* CardGetCurrentSongName(unsigned int *punLength);

extern u_char CardCheckCard(void);          // check by examining physical PIN
extern u_char CardCheckPresent(void);       // check by examining administration
//extern TError CardPlayMp3File(char *path);
extern void CardStopMp3File(void);
extern void CardUpdateTicks(void);
extern u_char CardGetNumberOfPlayLists(void);

void MmcPlayFile( void );



