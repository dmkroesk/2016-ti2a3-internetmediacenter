#ifndef _Inet_H
#define _Inet_H
/*
 *  Copyright STREAMIT BV, 2010.
 *
 *  Project             : SIR
 *  Module              : Inet
 *  File name  $Workfile: Inet.h  $
 *       Last Save $Date: 2006/02/24 13:46:55  $
 *             $Revision: 0.1  $
 *  Creation Date       : 2006/02/24 13:46:55
 *
 *  Description         :
 *
 */

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <sys/sock_var.h>

#include "typedefs.h"
#include "http.h"

/*--------------------------------------------------------------------------*/
/*  Constant definitions                                                    */
/*--------------------------------------------------------------------------*/
/*!\brief Options for the request header */
#define INET_FLAG_NO_AUTH               0x0001  /* Does not attempt authentication automatically. */
#define INET_FLAG_CLOSE                 0x0002  /* Request to close the connection when finished. */
#define INET_FLAG_ADD_SERIAL            0x0004  /* Check if this is a request that needs parameters (ends in '=').
                                                   If so, adds the serialnumber to the end of the URL. */
#define INET_FLAG_ICY_META_REQ          0x0008  /* Request meta data (Only Shoutcast/Icecast) */
/*#define INET_FLAG_NO_AUTO_REDIRECT      0x0100  * Does not automatically handle redirection in HttpSendRequest. */
/*#define INET_FLAG_PRAGMA_NOCACHE        0x0200  * Instruct proxies to bypass its cache. */

/*!\brief Query Info Flags. Attributes. Info to retrieve from the HTTP response */
#define INET_HTTP_QUERY_STATUS_CODE     0x0001  /* Get the status code returned by the server. */
#define INET_HTTP_QUERY_CONTENT_LENGTH  0x0002  /* Get the size of the resource, in bytes. */
#define INET_HTTP_QUERY_CONTENT_TYPE    0x0004  /* Get the content type of the resource (such as text/html). */
#define INET_HTTP_QUERY_LOCATION        0x0008  /* Get the absolute Uniform Resource Identifier (URI) used in a Location response-header. */
#define INET_HTTP_QUERY_ICY_METADATA    0x0010  /* Get the metadata interval */
/*#define INET_HTTP_QUERY_CUSTOM          0x0100  * Search for the header name specified in pcInfo and store the header data in pcInfo. */
/*#define INET_HTTP_QUERY_RAW_HEADERS     0x0200  * Get all headers returned by the server. Each header is separated by a carriage return/line feed (CR/LF) sequence. */

/*!\brief Query Info Flags. Modifiers. Info to retrieve from the HTTP response */
#define INET_HTTP_QUERY_MOD_NUMERIC     0x1000  /* Returns the data as a 32-bit number for headers whose value is a number, such as the status code. */

/*!\brief Protocol values in bProto */
#define INET_PROTO_UNKNOWN              0
#define INET_PROTO_HTTP                 1
#define INET_PROTO_ICY                  2

/*!\brief Mime Types */
#define MIME_TYPE_UNKNOWN               0
/*!\brief Mime Types for Audio */
#define MIME_TYPE_MP3                   1
#define MIME_TYPE_WMA                   2
#define MIME_TYPE_AAC                   3
/*!\brief Mime Types for text */
#define MIME_TYPE_TEXT                  10
#define MIME_TYPE_HTML                  11
/*!\brief Mime Types for playlists */
#define MIME_TYPE_PLS                   20
#define MIME_TYPE_M3U                   21
#define MIME_TYPE_ASX                   22

/*--------------------------------------------------------------------------*/
/*  Type declarations                                                       */
/*--------------------------------------------------------------------------*/

/*! \brief State of this module */
typedef enum _TSTATE
{
    INET_STATE_IDLE = 0,        /* We are idle */
    INET_STATE_BUSY,            /* We are busy with the handle */
    INET_STATE_CLOSING,         /* We are returning to idle */
} TState;

/*! \brief Retry counters (are kept per host) */
typedef struct _TRetries
{
    unsigned char byNoDnsCount;
    unsigned char byNoConnectCount;
    unsigned char byBadResponseCount;
} TRetries;

/*! \brief Internet request */
typedef struct _INETREQ
{
    char            *pszRequest;
    unsigned int    unRequestInUse;
    unsigned int    unRequestBufSize;
    char            *pszResponse;
    unsigned int    unResponseInUse;
    unsigned int    unResponseBufSize;
    unsigned short  wOptions;
    unsigned short  wHttpMode;
    char            byProto;
} INETREQ;

/*! \brief Request handle */
typedef INETREQ *   HINETREQ;

/*! \brief Internet handle contents */
typedef struct _INET
{
    char            *pszUrl;
    TUrlParts       tUrlParts;
    unsigned long   ulIpAddress;
    unsigned short  wPort;
    unsigned long   ulRecvTimeout;
    unsigned int    unMss;
    unsigned int    unTcpRecvBufSize;
    TCPSOCKET       *ptSocket;
    FILE            *ptStream;
    TRetries        tRetries;
    TState          tState;
    HINETREQ        hRequest;
} INET;

/*! \brief Internet handle */
typedef INET *      HINET;


/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  Global functions                                                        */
/*--------------------------------------------------------------------------*/

/*!
 * \brief Initializes an application's use of the Inet functions.
 *
 * This is the first Inet function that should be called by an application.
 * After the application has finished using the TInetHandle, it must be
 * closed using the InetClose function.
 *
 * \param   pszAgent [in] Pointer to a null-terminated string that specifies
 *          the name of the application or entity calling the Inet functions.
 *          This name is used as the user agent in HTTP based protocols.
 *
 * \return  A handle that the application passes to subsequent Inet functions.
 *          On failure, it returns NULL.
 */
extern HINET InetOpen(void);

/*!
 * \brief Opens an Internet session for a given site.
 *
 * \param   hInet [in] Handle returned by a previous call to InternetOpen.
 * \param   pszUrl [in] Pointer to a null-terminated string that specifies
 *          the host to connect to. This may be a complete URI, in which case
 *          only the scheme specifier and the host part are used.
 * \param   ulRecvTimeout [in] Wait time (in ms) when performing operations on
 *          this connection. If this time elapses an error is returned.
 *          Pass 0 to use the built-in default of 5000ms.
 * \param   unMss [in] TCP Maximum Segment Size. Packets above this limit are
 *          fragmented (which NutOS cannot handle..).
 *          Pass 0 to use the built-in default of 536.
 * \param   unTcpRecvBufSize [in] TCP Window Size. The maximum amount of
 *          outstanding data.
 *          Pass 0 to use the built-in default of 5xMSS.
 *
 * \return  0 if the connection is successful.
 *          TError otherwise.
 */
extern TError InetConnect(HINET hInet, CONST char *pszUrl, unsigned long ulRecvTimeout, unsigned int unMss, unsigned int unTcpRecvBufSize);

/*!
 * \brief Create a new request to be sent to an Internet server.
 *
 * When called mutliple times on the same inet handle, the previous request
 * will be overwritten with the new request.
 *
 * \param   hInet [in] Handle returned by a previous call to InternetOpen.
 * \param   pszMethod [in] A pointer to a null-terminated string that contains
 *          the method to use in the request. If this parameter is NULL, the
 *          function uses GET.
 * \param   pszPath [in] A pointer to a null-terminated string that contains
 *          the path to act upon.
 * \param   pszAccept[in] A pointer to a null-terminated string that indicates
 *          the media types accepted by the client. If this parameter is NULL,
 *          a string that indicates that all types are accepted is sent to the
 *          server.
 * \param   wOptions [in] This parameter can be any of the following values:
 *          INET_FLAG_NO_AUTH           Does not attempt authentication automatically.
 *          INET_FLAG_CLOSE             Request to close the connection when finished.
 *          INET_FLAG_ADD_SERIAL        Check if this is a request that needs parameters (ends in '=').
 *                                      If so, adds the serialnumber to the end of the URL.
 *          INET_FLAG_ICY_META_REQ      Add a request for meta data to the header.
 *
 *          Not implemented yet:
 *          INET_FLAG_NO_AUTO_REDIRECT  Does not automatically handle redirection in HttpSendRequest.
 *          INET_FLAG_PRAGMA_NOCACHE    Instruct proxies to bypass its cache.
 *
 * \return  0 on success.
 *          TError otherwise.
 */
extern TError InetHttpOpenRequest(HINET hInet, CONST char *pszMethod, CONST char *pszPath, CONST char *pszAccept, unsigned short wOptions);

/*!
 * \brief Adds one or more HTTP request headers to the HTTP request handle.
 *
 * InetHttpAddRequestHeaders appends additional, free-format headers to the HTTP request
 * handle and is intended for use by sophisticated clients that need detailed control
 * over the exact request sent to the HTTP server.
 *
 * \param   hInet [in] Handle returned by a previous call to InternetOpen.
 * \param   pszHeaders [in] Pointer to a string variable containing the headers
 *          to append to the request. Each header must be terminated by a CR/LF pair.
 *
 * \return  0 on success.
 *          -1 otherwise.
 */
extern int InetHttpAddRequestHeaders(HINET hInet, CONST char *pszHeaders);

/*!
 * \brief Sends the specified request to the HTTP server.
 *
 * After the request is sent, the status code and response headers from the HTTP
 * server are read.
 * All response headers are maintained internally and are available to client
 * applications through the HttpQueryInfo function.
 *
 * Any redirections/authentications and or retries are handled by this function
 * unless specified otherwise by the application.
 *
 * An application can use the same HTTP request handle in multiple calls to
 * this function, but the application must make sure that all data that is returned
 * by the server is read before calling this function again.
 *
 * \future  maybe: The function also lets the client specify optional data to send
 *                 to the HTTP server immediately following the request headers.
 *                 This feature is generally used for "write" operations such as PUT and POST.
 *
 * \param   hInet [in] Handle returned by a previous call to InternetOpen.
 *
 * \return  0 on success.
 *          TError otherwise.
 */
extern TError InetHttpSendRequest(HINET hInet);


/*!
 * \brief Retrieves a specified response header from the HTTP server's response.
 *
 * The function also lets the client specify optional data to send to the HTTP server
 * immediately following the request headers. This feature is generally used for "write"
 * operations such as PUT and POST.
 *
 * \param   hInet [in] Handle returned by a previous call to InternetOpen.
 * \param   wInfoLevel [in] Combination of an attribute to be retrieved and flags that
 *          modify the request. For a list of possible attribute and modifier values, see Query Info Flags:
 * Attributes
 *     The attribute flags are used to indicate what data to retrieve. Most of the attribute flags map directly to a specific HTTP header.
 *     There are also some special flags, such as HTTP_QUERY_RAW_HEADERS, that are not related to a specific header.
 *
 *     INET_HTTP_QUERY_STATUS_CODE         Receives the status code returned by the server
 *     INET_HTTP_QUERY_CONTENT_LENGTH      Retrieves the size of the resource, in bytes.
 *     INET_HTTP_QUERY_CONTENT_TYPE        Receives the content type of the resource (such as text/html).
 *     INET_HTTP_QUERY_LOCATION            Retrieves the absolute Uniform Resource Identifier (URI) used in a Location response-header.
 *
 *     Not implemented yet:
 *     INET_HTTP_QUERY_CUSTOM              Causes InetHttpQueryInfo to search for the header name specified in pcInfo and store the header data in pcInfo.
 *     INET_HTTP_QUERY_RAW_HEADERS_CRLF    Receives all the headers returned by the server. Each header is separated by a carriage return/line feed (CR/LF) sequence.
 *
 * Modifiers
 *     The modifier flags are used in conjunction with an attribute flag to modify the request.
 *     Modifier flags either modify the format of the data returned or indicate where to search for the data.
 *
 *     INET_HTTP_QUERY_MOD_FLAG_NUMBER     Returns the data as a signed long number for headers whose value is a number, such as the status code.
 *
 * \param   pInfo [in, out] Address of a Pointer to a buffer to receive the requested information.
 *          This parameter must not be NULL.
 * \param   punInfoSize [in, out] Pointer to a variable that contains, on entry, the
 *          size in bytes of the buffer pointed to by pInfo or 0 to let this function
 *          allocate memory to pInfo.
 *          When the function returns successfully, this variable contains the number of
 *          bytes of information written to the buffer.
 *          When the function returns with an error, this parameter contains the size,
 *          in bytes, that the buffer behind pInfo should be.
 *          The calling application can then allocate a buffer of this size or larger,
 *          and call the function again.
 * \param   pnIndex [in, out] Pointer to a zero-based header index used to enumerate
 *          multiple headers with the same name. When calling the function, this parameter
 *          is the index of the specified header to return. When the function returns,
 *          this parameter is the index of the next header. If the next index cannot be
 *          found, -1 is returned.
 *          Not implemented yet. Currently this parameter is ignored!
 *
 * \return  < 0 on errors
 *          0 if we could not find the requested info
 *          > 0 if we found the info. pInfo now contains the requested data
 */
extern int InetHttpQueryInfo(HINET hInet, unsigned short wInfoLevel, void **pInfo, unsigned int *punInfoSize, int *pnIndex);

/*!
 * \brief Determine the type of data we get from the server
 *
 * \param   hInet [in] Handle returned by a previous call to InternetOpen.
 *
 * \return  The MIME type
 */
extern int InetGetMimeType(HINET hInet);

/*!
 * \brief Read data from an Internet connection.
 *
 * \param   hInet [in] Handle to read the data from
 * \param   pcBuf [in] The received data is stored there,
 *          upto unBufSize bytes.
 * \param   unBufSize [in] Upto this many bytes are stored.
 *
 * \return  If successful, the number of received data bytes
 *          is returned. This may be less than the specified
 *          size of the buffer. The return value 0 indicates
 *          a timeout, while -1 is returned in case of an error
 *          or broken connection.
 */
extern int InetRead(HINET hInet, char *pcBuf, unsigned int unBufSize);

/*!
 * \brief Read data from an Internet connection.
 *
 * \param   hInet [in] Handle to read the data from.
 * \param   pbyBuf [in] The received data is stored there,
 *          unBufSize bytes.
 * \param   unBufSize [in] This many bytes are stored.
 *
 * \return  If successful, the number of received data bytes
 *          is returned (which should be unBufSize bytes).
 *          On error -1 is returned.
 */
extern int InetReadExact(HINET hInet, unsigned char *pbyBuf, unsigned int unBufSize);

/*!
 * \brief Read data from an Internet connection.
 *
 * The data is stored in a buffer which is allocated.
 * Reading continues until there is no more memory,
 * a timeout, or the server disconnects.
 *
 * \note It is advisable to set your read timeout to 30s or 60s
 *       to prevent this routine from returning too soon.
 *
 * \param   hInet [in] Handle to read the data from
 * \param   ppcBuf [in, out] Adres of a pointer. If on input the pointer is null,
 *          a new memory area is allocated and this parameter receives the address
 *          of that memory area.
 *          If the pointer is non-null, the received data is stored there,
 *          upto punBufSize bytes.
 * \param   punBufSize [in, out] On input, upto this many bytes are stored.
 *          On output, receives the size of the allocated memory behind ppcBuf.
 *
 * \return  The amount of data read from the connection.
 */
extern int InetReadFile(HINET hInet, char **ppcBuf, unsigned int *punBufSize);

/*!
 * \brief Closes an Inet handle
 *
 * The function terminates any pending operations on the handle and discards
 * any outstanding data.
 * It is safe to call this function from one thread while another thread
 * is using this request. This will unblock the first thread.
 *
 * \param   hInet [in] Handle returned by a previous call to InternetOpen.
 *
 * \return  NULL
 */
extern HINET InetClose(HINET hInet);

#endif /* _Inet_H */
