#ifndef TIMECALC_H_
#define TIMECALC_H_

extern void addToClock(struct _tm *tm, int years, int months, int days, int hours, int minutes, int seconds);
extern int 	clockComparator(struct _tm *current, struct _tm *comparedTime);

#endif /* _TIMECALC_H */
