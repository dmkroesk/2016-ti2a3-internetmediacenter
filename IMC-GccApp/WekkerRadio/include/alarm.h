#ifndef ALARM_H_
#define ALARM_H_

extern void activateAlarm(void);
extern int	alarmActivated(void);
extern void setAlarm(tm *time);
extern void ResetAlarm( void );

#endif /* _ALARM_H */

