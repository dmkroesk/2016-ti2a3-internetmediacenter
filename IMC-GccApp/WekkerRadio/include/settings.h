/*
 * settings.h
 *
 * Created: 26-2-2016 13:29:34
 *  Author: Bart
 */ 


#ifndef SETTINGS_H_
#define SETTINGS_H_


tm* SettingsGetDateTime( void );
void SettingsSetDateTime( tm* );

void SetVolumeChangeBoolean( int* );

int SettingsGetSnoozeTime( void );
void SettingsSetSnoozeTime( int );
int SettingsGetSnoozeTimeChangeBoolean( void );
void SettingsSnoozeTimeChangeBooleanTrue( void );
void SettingsSnoozeTimeChangeBooleanFalse( void );

int SettingsGetVolume( void );
void SettingsSetVolume( int );

int SettingsGetVolumeChangeBoolean ( void );
void SettingsVolumeChangeBooleanTrue ( void );
void SettingsVolumeChangeBooleanFalse ( void );

int SettingsGetAlarmTimeChangeBoolean ( void );
void SettingsAlarmTimeChangeBooleanTrue ( void );
void SettingsAlarmTimeChangeBooleanFalse ( void );

void VolumeUp( void );
void VolumeDown( void );

void AlarmTimeUp ( void );
void AlarmTimeDown ( void );

void SnoozeTimeUp ( void );
void SnoozeTimeDown ( void );

void SetDefaultSettings( void );

void SettingsSaveTimeZone(int _utc);

int SettingsLoadTimeZone( void );

void SetBeep( int );
int* GetBeep( void );

int LoadAlarmTime(int);
void SaveAlarmTime(void);
void ShowAlarmTime(void);

void SettingsLoadSettings(void);
void SettingsSaveSettings(void);

int SettingsGetPowerState( void );
void SettingsSetPowerState( int );

#endif /* SETTINGS_H_ */
