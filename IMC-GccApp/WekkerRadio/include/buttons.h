/*
 * buttons.h
 *
 * Created: 25-2-2016 11:57:18
 *  Author: Bart en Karel
 */ 


#ifndef BUTTONS_H_
#define BUTTONS_H_

void buttonSetFunctEsc(void *);
void buttonSetFunctOK(void *);
void buttonSetFunctUp(void *);
void buttonSetFunctDown(void *);
void buttonSetFunctLeft(void *);
void buttonSetFunctRight(void *);

void buttonSetFunct1(void *);
void buttonSetFunct2(void *);
void buttonSetFunct3(void *);
void buttonSetFunct4(void *);
void buttonSetFunct5(void *);
void buttonSetFunctAlt(void *);
void buttonLoopFunction ( void );

void buttonResetAll( void );


#endif /* BUTTONS_H_ */
