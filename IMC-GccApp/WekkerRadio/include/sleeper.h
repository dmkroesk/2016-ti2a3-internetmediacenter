#ifndef SLEEP_H_
#define SLEEP_H_

void SetSleepTimer( void );
void StopSleepTimer( void );
void SetTime( void );
void ResetSleep( void );
void ButtonPressed( void );

extern tm  		setTime;
extern tm 		currentTime;
extern int 		periodInMinutes;
extern int 		isRunning;

#endif /* _SLEEP_H_ */

