/*
 * AudioSettingsMenu.h
 *
 * Created: 30-03-16 12:01:00
 *  Author: Karel
 */ 


#ifndef AUDIOSETTINGSMENU_H_
#define AUDIOSETTINGSMENU_H_

void ShowMenuAudio( void );

void TrebleUp ( void );
void TrebleDown ( void );
void BassUp ( void );
void BassDown( void );

int VsSetBassReg( u_char );
u_short VsGetBassReg( void );


#endif /* AUDIOSETTINGSMENU_H_ */
