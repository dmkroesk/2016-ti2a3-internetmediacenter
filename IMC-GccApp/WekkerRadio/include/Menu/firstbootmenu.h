/*
 * FirstBootMenu.h
 *
 * Created: 26-2-2016 10:28:22
 *  Author: Bart
 */ 


#ifndef FIRSTBOOTMENU_H_
#define FIRSTBOOTMENU_H_

void initFirstBoot( void );
void resetTimeZone( void );
void doFirstBoot( void );

void showTimeZone( int );

void SaveTimeZone( int );
int LoadTimeZone( void );

#endif /* FirstBootMenu_H_ */
