/*
 * timeStampMenu.h
 *
 * Created: 26-02-16 13:46:06
 *  Author: Karel
 */ 


#ifndef TIMESTAMPMENU_H_
#define TIMESTAMPMENU_H_

void InitTimeStampMenu( tm* );
void DisplayTimeStamp( void );
void SetInMainMenu( void );


#endif /* TIMESTAMPMENU_H_ */

