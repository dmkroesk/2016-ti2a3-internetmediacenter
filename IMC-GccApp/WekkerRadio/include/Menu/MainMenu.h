/*
 * MainMenu.h
 *
 * Created: 2/26/2016 9:19:12 AM
 *  Author: Tom Remeeus
 */ 


#ifndef MAINMENU_H_
#define MAINMENU_H_

#include "Menu/ScheduleMenu.h"
#include "Menu/SettingsMenu.h"
#include "Menu/PlayMenu.h"
#include "Menu/PlaySubMenu/PlaylistMenu.h"
#include "Menu/SettingsSubMenu/TimeSettingsMenu.h"
#include "Menu/SettingsSubMenu/AudioSettingsMenu.h"
#include "Menu/AlarmSubMenu/AlarmNewMenu.h"
#include "Menu/PlaySubMenu/SDCardMenu.h"
#include "display.h"
#include "buttons.h"
#include "alarm.h"
#include "Menu/PlaySubMenu/StreamMenu.h"

void ShowMenuMain( void );

#endif /* MAINMENU_H_ */
