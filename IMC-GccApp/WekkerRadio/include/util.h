#ifndef _Util_H
#define _Util_H
/*
 *  Copyright STREAMIT BV, 2010.
 *
 *  Project             : SIR
 *  Module              : Util
 *  File name  $Workfile: Util.h  $
 *       Last Save $Date: 2006/05/11 9:54:52  $
 *             $Revision: 0.1  $
 *  Creation Date       : 2006/05/11 9:54:52
 *
 *  Description         : Utility functions for the SIR project
 *
 */

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>

#include <sys/heap.h>

/*--------------------------------------------------------------------------*/
/*  Constant definitions                                                    */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  Type declarations                                                       */
/*--------------------------------------------------------------------------*/
/*!\brief A structure for holding tags and its descriptor.
 * By convention the last element of this struct is indicated by
 * a NULL pszTag. pDesc of the last element is free to be used by the user
 * for his own purposes.
 */
typedef struct _tLut
{
    PGM_P   pszTag;
    void    *pDesc;
} tLut;

/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  Global functions                                                        */
/*--------------------------------------------------------------------------*/

extern void *MyMalloc(unsigned int unSize);

/*!
 * \brief Free previously allocated memory.
 *
 * The pointer is set to NULL, so you can easily detect if there
 * is allocated memory behind the pointer.
 *
 * \param   p [in,out] Pointer to a (char) memory block.
 *
 * \return  -
 */
#define     MyFree(p)               \
            {                       \
                if ((p) != NULL)    \
                {                   \
                    NutHeapFree(p); \
                    (p) = NULL;     \
                }                   \
            }

extern char *strdup(CONST char *str);

extern int  BufferMakeRoom(char **ppcBuf, unsigned int *punBufSize, unsigned int unBufInUse, unsigned int unSizeNeeded);
extern int  BufferAddString(char **ppcBuf, unsigned int *punBufSize, unsigned int *punBufInUse, CONST char *pszString);

extern void *LutSearch(CONST tLut tLookupTable[], CONST char *pcText, unsigned char byLen);

#endif /* _Util_H */
