/*
 * ntp.h
 *
 * Created: 10-3-2016 16:17:44
 *  Author: Bart
 */ 


#ifndef NTP_H_
#define NTP_H_

int GetTimeFromNTP(char[], tm *);

extern void SetTimeZone(int zoneCode);



#endif /* NTP_H_ */
