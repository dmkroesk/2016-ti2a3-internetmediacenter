/* ========================================================================
 * [PROJECT]    SIR100
 * [MODULE]     Display
 * [TITLE]      display header file
 * [FILE]       display.h
 * [VSN]        1.0
 * [CREATED]    030414
 * [LASTCHNGD]  030414
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    API and gobal defines for display module
 * ======================================================================== */

#ifndef _Display_H
#define _Display_H

#include "rtc.h"

/*-------------------------------------------------------------------------*/
/* global defines                                                          */
/*-------------------------------------------------------------------------*/
#define DISPLAY_SIZE                16
#define NROF_LINES                  2
#define MAX_SCREEN_CHARS            (NROF_LINES*DISPLAY_SIZE)

#define LINE_0                      0
#define LINE_1                      1

#define LINE_0_1                                0x80
#define LINE_0_2                                0x81
#define LINE_0_3                                0x82
#define LINE_0_4                                0x83
#define LINE_0_5                                0x84
#define LINE_0_6                                0x85
#define LINE_0_7                                0x86
#define LINE_0_8                                0x87
#define LINE_0_9                                0x88
#define LINE_0_10                               0x89
#define LINE_0_11                               0x8A
#define LINE_0_12                               0x8B
#define LINE_0_13                               0x8C
#define LINE_0_14                               0x8D
#define LINE_0_15                               0x8E
#define LINE_0_16                               0x8F

#define LINE_1_1                                0xC0
#define LINE_1_2                                0xC1
#define LINE_1_3                                0xC2
#define LINE_1_4                                0xC3
#define LINE_1_5                                0xC4
#define LINE_1_6                                0xC5
#define LINE_1_7                                0xC6
#define LINE_1_8                                0xC7
#define LINE_1_9                                0xC8
#define LINE_1_10                               0xC9
#define LINE_1_11                               0xCA
#define LINE_1_12                               0xCB
#define LINE_1_13                               0xCC
#define LINE_1_14                               0xCD
#define LINE_1_15                               0xCE
#define LINE_1_16                               0xCF

#define FIRSTPOS_LINE_0             0
#define FIRSTPOS_LINE_1             0x40


#define LCD_BACKLIGHT_ON            1
#define LCD_BACKLIGHT_OFF           0

#define ALL_ZERO          			0x00      // 0000 0000 B
#define WRITE_COMMAND     			0x02      // 0000 0010 B
#define WRITE_DATA        			0x03      // 0000 0011 B
#define READ_COMMAND      			0x04      // 0000 0100 B
#define READ_DATA         			0x06      // 0000 0110 B


/*-------------------------------------------------------------------------*/
/* typedefs & structs                                                      */
/*-------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  Global variables                                                        */
/*--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
extern void LcdChar(char);
extern void LcdBackLight(u_char);
extern void LcdInit(void);
extern void LcdLowLevelInit(void);
extern void LcdWriteText(char[]);
extern void LcdWriteTextOverride(char[]);
extern void ClearScreen(void);
extern void writeDataToLocation(u_char location, u_char letter);
extern void writeLineOne(char[]);
extern void writeLineTwo(char[]);
extern void clearLineOne( void );
extern void clearLineTwo( void );
extern void writeLinesFirstLineInCapitals(char[], char[]);
extern void getTimeString(struct _tm *time, char * str);
extern char charFromNumber(int number);
extern char charToCapitalIfPossible(char c);
extern char * charArrayToCapitalsIfPossible(char c[]);
extern char *splitMultiDigitNumberToCharArray(int number, int digits, char * numms);
extern int POW(int number, int power);
extern void MoveUpOrDown(int charAmount);
extern void MoveLeftOrRight(int charAmount);
extern void WriteTextToLcd(void);

#endif /* _Display_H */
/*  様様  End Of File  様様様様 様様様様様様様様様様様様様様様様様様様様様様 */





