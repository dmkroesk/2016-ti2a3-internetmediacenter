/* ========================================================================
* [PROJECT]    SIR100
* [MODULE]     Display
* [TITLE]      display source file
* [FILE]       display.c
* [VSN]        1.0
* [CREATED]    26092003
* [LASTCHNGD]  06102006
* [COPYRIGHT]  Copyright (C) STREAMIT BV
* [PURPOSE]    contains all interface- and low-level routines to
*              control the LCD and write characters or strings (menu-items)
* ======================================================================== */

#define LOG_MODULE  LOG_DISPLAY_MODULE

#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"
#include "rtc.h"
#include "keyboard.h"

// Alle chars uit een string returnen
#define FOREACHCHAR(String, C) int counter = 0;\
for (C = String[counter]; C != NULL; counter++, C = String[counter])

#define SCROLL_HORIZONTAL					0
#define SCROLL_HORIZONTAL_ONLY_LINE_1		1

/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
char *	  currentText;      // The latest received text
// When scrolling horizontal
int       startPosition;		// Text start position
// When scrolling vertical
int		  secondLineStartPosition;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void LcdWriteByte(u_char, u_char);
static void LcdWriteNibble(u_char, u_char);
static void LcdWaitBusy(void);
// args: scroll direction
THREAD(scrollDisplay, args){
	// Get our scroll type (Up or Down)
	char scrollType = (char)args;
	LogMsg_P(LOG_INFO, PSTR("Scroll Mode, Press Esc to Exit"));
	
	int counter;
	int initialCounter;
	initialCounter = strlen(currentText) - secondLineStartPosition - 16;

	counter = initialCounter;
	if (scrollType == SCROLL_HORIZONTAL){
		while (counter > 0){
			MoveUpOrDown(16);
			NutSleep(250);
			counter--;
		}
		} else {
		while (counter > 0){
			MoveLeftOrRight(1);		// Going Right
			NutSleep(400);
			counter--;
			if (counter == 0){
				counter = initialCounter;
			}
		}
	}
	NutThreadExit();
	NutThreadDestroy();

	for(;;) {
		NutSleep(10);
	}
}

/*!
* \addtogroup Display
*/

/*@{*/

	/*-------------------------------------------------------------------------*/
	/*                         start of code                                   */
	/*-------------------------------------------------------------------------*/

	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	/*!
	* \brief control backlight
	*/
	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */


	void LcdBackLight(u_char Mode)
	{
		if (Mode==LCD_BACKLIGHT_ON)
		{
			sbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn on backlight
		}

		if (Mode==LCD_BACKLIGHT_OFF)
		{
			cbi(LCD_BL_PORT, LCD_BL_BIT);   // Turn off backlight
		}
	}

	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	/*!
	* \brief Write a single character on the LCD
	*
	* Writes a single character on the LCD on the current cursor position
	*
	* \param LcdChar character to write
	*/
	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	void LcdChar(char MyChar)
	{
		LcdWriteByte(WRITE_DATA, MyChar);
	}


	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	/*!
	* \brief Low-level initialisation function of the LCD-controller
	*
	* Initialise the controller and send the User-Defined Characters to CG-RAM
	* settings: 4-bit interface, cursor invisible and NOT blinking
	*           1 line dislay, 10 dots high characters
	*
	*/
	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	void LcdLowLevelInit()
	{
		u_char i;

		NutDelay(140);                               // wait for more than 140 ms after Vdd rises to 2.7 V

		for (i=0; i<3; ++i)
		{
			LcdWriteNibble(WRITE_COMMAND, 0x33);      // function set: 8-bit mode; necessary to guarantee that
			NutDelay(4);                              // SIR starts up always in 5x10 dot mode
		}

		LcdWriteNibble(WRITE_COMMAND, 0x22);        // function set: 4-bit mode; necessary because KS0070 doesn't
		NutDelay(1);                                // accept combined 4-bit mode & 5x10 dot mode programming

		//LcdWriteByte(WRITE_COMMAND, 0x24);        // function set: 4-bit mode, 5x10 dot mode, 1-line
		LcdWriteByte(WRITE_COMMAND, 0x28);          // function set: 4-bit mode, 5x7 dot mode, 2-lines
		NutDelay(5);

		LcdWriteByte(WRITE_COMMAND, 0x0C);          // display ON/OFF: display ON, cursor OFF, blink OFF
		NutDelay(5);

		LcdWriteByte(WRITE_COMMAND, 0x01);          // display clear
		NutDelay(5);

		LcdWriteByte(WRITE_COMMAND, 0x06);          // entry mode set: increment mode, entire shift OFF


		LcdWriteByte(WRITE_COMMAND, 0x80);          // DD-RAM address counter (cursor pos) to '0'
	}


	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	/*!
	* \brief Low-level routine to write a byte to LCD-controller
	*
	* Writes one byte to the LCD-controller (by  calling LcdWriteNibble twice)
	* CtrlState determines if the byte is written to the instruction register
	* or to the data register.
	*
	* \param CtrlState destination: instruction or data
	* \param LcdByte byte to write
	*
	*/
	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	static void LcdWriteByte(u_char CtrlState, u_char LcdByte)
	{
		LcdWaitBusy();                      // see if the controller is ready to receive next byte
		LcdWriteNibble(CtrlState, LcdByte & 0xF0);
		LcdWriteNibble(CtrlState, LcdByte << 4);
	}

	void LcdWriteText(char c[])
	{	// No text present
		if (currentText == ""){
			currentText = malloc(sizeof(c));
			currentText = c;
			startPosition = 0;
		} // Append to old text
		else {
			int oldSize;
			oldSize = sizeof(currentText);

			currentText = malloc(sizeof(currentText) + sizeof(c));

			int counter;
			counter = oldSize;

			while (counter < sizeof(currentText)){
				currentText[counter] = c[counter - oldSize];
				counter++;
			}
		}

		WriteTextToLcd();
		
		NutThreadCreate("scrollDisplay", scrollDisplay, SCROLL_HORIZONTAL, 512);
		return;
	}
	// Used by LcdWriteText
	void WriteTextToLcd(){
		ClearScreen();

		int x;
		writeDataToLocation(LINE_0_1, currentText[startPosition]);
		
		for (x = startPosition + 1; x < strlen(currentText) && x < 16 + startPosition; x++)
		{
			if (!(currentText[x] == '\0'))
			{
				LcdChar(currentText[x]);
			}
		}

		if (strlen(currentText) > 16 + startPosition){
			writeDataToLocation(LINE_1_1, currentText[15 + startPosition]);
			for (x = startPosition + 16; x < strlen(currentText) && x < 33 + startPosition; x++)
			{
				if (!(currentText[x] == '\0'))
				{
					LcdChar(currentText[x]);
				}
			}
		}
		//LogMsg_P(LOG_INFO, PSTR("Reached end"));

		return;
	}

	void writeLineOne(char *c){
		clearLineOne();
		if (c[0] != '\0'){
			writeDataToLocation(LINE_0_1, c[0]);

			int x;
			for (x = 1; x < strlen(c); c++){
				if (!(c[x] == '\0'))
				{
					LcdChar(c[x]);
				}
			}
		}
	}

	void writeLineTwo(char *c){
		clearLineTwo();
		currentText = calloc(sizeof(c) / sizeof(char), sizeof(char));
		currentText = c;

		if (c[0] != '\0'){
			writeDataToLocation(LINE_1_1, c[secondLineStartPosition]);
			int x;
			for (x = 1 + secondLineStartPosition; x < strlen(c); c++){
				if (!(c[x] == '\0'))
				{
					LcdChar(c[x]);
				}
			}
		}
	}

	void writeLinesFirstLineInCapitals(char c[], char d[]){
		// Capitals for first line
		c = charArrayToCapitalsIfPossible(c);
		// Clear screen
		ClearScreen();
		// Write both lines
		secondLineStartPosition = 0;
		writeLineOne(c);
		writeLineTwo(d);
		// Create the thread
		NutThreadCreate("scrollDisplay", scrollDisplay, (int*)SCROLL_HORIZONTAL_ONLY_LINE_1, 512);
	}

	void LcdWriteTextOverride(char c[]){
		// Clr screen first
		startPosition = 0; // Reset our line number
		currentText = "";
		LcdWriteText(c);
	}

	void MoveUpOrDown(int charAmount){

		if (startPosition + charAmount >= strlen(currentText) || startPosition + charAmount < 0)
		startPosition = 0;
		else
		startPosition = startPosition + charAmount;

		//LogMsg_P(LOG_INFO, PSTR("OKE"));
		WriteTextToLcd();
		return;
	}

	void MoveLeftOrRight(int charAmount){

		if (secondLineStartPosition + charAmount >= 0 && secondLineStartPosition + charAmount + 15 <= strlen(currentText)){
			secondLineStartPosition = secondLineStartPosition + charAmount;
			
			} else {
			secondLineStartPosition = 0;
		}
		writeLineTwo(currentText);

	}

	void ClearScreen(){
		// Clr screen
		LcdWriteByte(WRITE_COMMAND, 0x01);
	}

	void clearLineOne(){
		
		writeDataToLocation(LINE_0_1, ' ');

		int x;
		for (x = 0; x < 16; x++){
			LcdChar(' ');
		}
	}

	void clearLineTwo(){
		writeDataToLocation(LINE_1_1, ' ');

		int x;
		for (x = 0; x < 16; x++){
			LcdChar(' ');
		}
	}

	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	/*!
	* \brief Low-level routine to write a nibble to LCD-controller
	*
	* Writes a nibble to the LCD-controller (interface is a 4-bit databus, so
	* only 4 databits can be send at once).
	* The nibble to write is in the upper 4 bits of LcdNibble
	*
	* \param CtrlState destination: instruction or data
	* \param LcdNibble nibble to write (upper 4 bits in this byte
	*
	*/
	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	static void LcdWriteNibble(u_char CtrlState, u_char LcdNibble)
	{
		outp((inp(LCD_DATA_DDR) & 0x0F) | 0xF0, LCD_DATA_DDR);  // set data-port to output again

		outp((inp(LCD_DATA_PORT) & 0x0F) | (LcdNibble & 0xF0), LCD_DATA_PORT); // prepare databus with nibble to write

		if (CtrlState == WRITE_COMMAND)
		{
			cbi(LCD_RS_PORT, LCD_RS);     // command: RS low
		}
		else
		{
			sbi(LCD_RS_PORT, LCD_RS);     // data: RS high
		}

		sbi(LCD_EN_PORT, LCD_EN);

		asm("nop\n\tnop");                    // small delay

		cbi(LCD_EN_PORT, LCD_EN);
		cbi(LCD_RS_PORT, LCD_RS);
		outp((inp(LCD_DATA_DDR) & 0x0F), LCD_DATA_DDR);           // set upper 4-bits of data-port to input
		outp((inp(LCD_DATA_PORT) & 0x0F) | 0xF0, LCD_DATA_PORT);  // enable pull-ups in data-port
	}

	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	/*!
	* \brief Low-level routine to see if the controller is ready to receive
	*
	* This routine repeatetly reads the databus and checks if the highest bit (bit 7)
	* has become '0'. If a '0' is detected on bit 7 the function returns.
	*
	*/
	/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
	static void LcdWaitBusy()
	{
		u_char Busy = 1;
		u_char LcdStatus = 0;

		cbi (LCD_RS_PORT, LCD_RS);              // select instruction register

		sbi (LCD_RW_PORT, LCD_RW);              // we are going to read

		while (Busy)
		{
			sbi (LCD_EN_PORT, LCD_EN);          // set 'enable' to catch 'Ready'

			asm("nop\n\tnop");                  // small delay
			LcdStatus =  inp(LCD_IN_PORT);      // LcdStatus is used elsewhere in this module as well
			Busy = LcdStatus & 0x80;            // break out of while-loop cause we are ready (b7='0')
		}

		cbi (LCD_EN_PORT, LCD_EN);              // all ctrlpins low
		cbi (LCD_RS_PORT, LCD_RS);
		cbi (LCD_RW_PORT, LCD_RW);              // we are going to write
	}

	void writeDataToLocation(u_char location, u_char letter){
		LcdWriteByte(WRITE_COMMAND, location);
		LcdWriteByte(WRITE_DATA, letter);
	}

	char * splitMultiDigitNumberToCharArray(int number, int digits, char *numms){
		
		numms = malloc(sizeof(char) * digits);
		
		int counter;
		for (counter = 0; counter < digits; counter++){
			numms[counter] = charFromNumber(0);
		}
		counter = 1;
		while (digits - counter >= 0){
			// Laatste getal
			numms[digits - counter] = charFromNumber(number % 10);
			number /= 10;
			counter++;
		}

		numms[digits - counter] = charFromNumber(number);

		return numms;
	}

	char charFromNumber(int number){
		return (char)(48 + number);
	}

	char *charArrayToCapitalsIfPossible(char c[]){
		int counter;
		// Create capitals from all
		for (counter = 0; counter < strlen(c); counter++){
			c[counter] = charToCapitalIfPossible(c[counter]);
		}

		return c;
	}
	// returns
	char charToCapitalIfPossible(char c){
		// is it a char from 'a' to 'z' ?
		if (c > 96 && c < 123){
			c -= 32; // to capital
		}

		return c;
	}

	/* ---------- end of module ------------------------------------------------ */

	/*@}*/
