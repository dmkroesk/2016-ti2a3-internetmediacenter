// Sleeper
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "Menu/MainMenu.h"
#include "alarm.h"
#include "settings.h"
#include "time.h"
#include "timecalc.h"
#include "settings.h"
#include "shoutcast.h"

#include "vs10xx.h"

#include <time.h>
#include "rtc.h"

#include "buttons.h"
#include "sleeper.h"

#define LOG_MODULE  LOG_MAIN_MODULE

tm  		setTime;
tm 			currentTime;
int 		periodInMinutes = 0;
int 		isRunning = FALSE;

THREAD(setSleeper, args){
	if (isRunning == TRUE){
		
		X12RtcGetClock(&currentTime);

		while (clockComparator(&setTime, &currentTime) == 1 && isRunning == TRUE){
			NutSleep(1000);	
			LogMsg_P(LOG_INFO, PSTR("WAITING %i | %i "), setTime.tm_min, currentTime.tm_min);
			X12RtcGetClock(&currentTime);
		}

		LogMsg_P(LOG_INFO, PSTR("Sleeper started."));
		int initialVolume = SettingsGetVolume();
		// Less Volume every minute
		for (; initialVolume > 0 ; initialVolume--){
			if (isRunning == TRUE){
				SettingsSetVolume(initialVolume);
				LogMsg_P(LOG_INFO, PSTR("VOL TO: %i"), SettingsGetVolume());
				NutSleep(10000);
			} else {
				NutThreadExit();
				NutThreadDestroy();
			}
		}
		isRunning = FALSE;
	}
	LogMsg_P(LOG_INFO, PSTR("Sleeper Stopped."));
	// Sleep?
	NutThreadExit();
	NutThreadDestroy();
	for(;;);
}


void SetSleepTimer(){
	if (isRunning == TRUE){
		StopSleepTimer();
	}

	isRunning = TRUE;
	NutThreadCreate("setSleeper", setSleeper, NULL, 512);
}

void StopSleepTimer(){
	// Thread 
	if (isRunning == TRUE){
		isRunning = FALSE;
		NutSleep(1000);
	}
}

void ResetSleep(){
	StopSleepTimer();
	SetSleepTimer();

}

void ButtonPressed(){
	if (isRunning == TRUE){
		ResetSleep();
	}
}
