/*
* ethernet.c
*
* Created: 3-3-2016 12:38:38
*  Author: Bart
*/

#define LOG_MODULE  LOG_MAIN_MODULE

#include <sys/timer.h>

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <pro/sntp.h>

#include "log.h"
#include "ethernet.h"
#include "display.h"
#include "settings.h"

#define ETH0_BASE	0xC300
#define ETH0_IRQ	5

#define OK			0
#define NOK			1

static char eth0IfName[9] = "eth0";

int InetInit(void)
{
	// not needed
	//	uint8_t mac_addr[6] = { 0x00, 0x06, 0x98, 0x30, 0x02, 0x76 };
	
	int result = OK;

	writeLineOne("STARTING DEVICE");

	// Registreer NIC device (located in nictrl.h)
	if( NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ) )
	{
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutRegisterDevice()"));
		result = NOK;
	}
	
	if( OK == result )
	{
		writeLineTwo("Connecting...");
		if( NutDhcpIfConfig(eth0IfName, NULL, 0) )
		{
			LogMsg_P(LOG_ERR, PSTR("Error: >> NutDhcpIfConfig()"));
			result = NOK;
		}
	}
	
	if( OK == result )
	{
		LogMsg_P(LOG_INFO, PSTR("Networking setup OK, new settings are:\n") );

		LogMsg_P(LOG_INFO, PSTR("if_name: %s"), confnet.cd_name);
		LogMsg_P(LOG_INFO, PSTR("ip-addr: %s"), inet_ntoa(confnet.cdn_ip_addr) );
		LogMsg_P(LOG_INFO, PSTR("ip-mask: %s"), inet_ntoa(confnet.cdn_ip_mask) );
		LogMsg_P(LOG_INFO, PSTR("gw     : %s"), inet_ntoa(confnet.cdn_gateway) );
	}
	writeLineTwo("Connected       ");
	NutSleep(1000);
	
	return result;
}

void GetNtpTime(char addr[]) {
	time_t ntpTime = mktime(SettingsGetDateTime());
	u_long ulongAddr = inet_addr(addr);
	NutSNTPGetTime(&ulongAddr, &ntpTime);
}
