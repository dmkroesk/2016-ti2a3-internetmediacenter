/*
* buttons.c
*
* Created: 25-2-2016 11:57:32
*  Author: Bart en Karel
*/

#define LOG_MODULE  LOG_MAIN_MODULE

#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/event.h>
#include <sys/timer.h>
#include <watchdog.h>

#include "log.h"

#include "buttons.h"
#include "display.h"
#include "keyboard.h"
#include "Menu/FirstBootMenu.h"
#include "settings.h"
#include "sleeper.h"

//Selection buttons
static void (*functEsc)(void);
static void (*functOK)(void);
static void (*functUp)(void);
static void (*functDown)(void);
static void (*functLeft)(void);
static void (*functRight)(void);

//Numeric buttons
static void (*funct1)(void);
static void (*funct2)(void);
static void (*funct3)(void);
static void (*funct4)(void);
static void (*funct5)(void);
static void (*functAlt)(void);

void dummyFunction( void );

void dummyFunction() {
	NutSleep(1);
}

//Set the buttons
void buttonSetFunctEsc(void (*pFunct)) {
	functEsc = pFunct;
}
void buttonSetFunctOK(void (*pFunct)) {
	functOK = pFunct;
}
void buttonSetFunctUp(void (*pFunct)) {
	functUp = pFunct;
}
void buttonSetFunctDown(void (*pFunct)) {
	functDown = pFunct;
}
void buttonSetFunctLeft(void (*pFunct)) {
	functLeft = pFunct;
}
void buttonSetFunctRight(void (*pFunct)) {
	functRight = pFunct;
}
void buttonSetFunct1(void (*pFunct)) {
	funct1 = pFunct;
}
void buttonSetFunct2(void (*pFunct)) {
	funct2 = pFunct;
}
void buttonSetFunct3(void (*pFunct)) {
	funct3 = pFunct;
}
void buttonSetFunct4(void (*pFunct)) {
	funct4 = pFunct;
}
void buttonSetFunct5(void (*pFunct)) {
	funct5 = pFunct;
}
void buttonSetFunctAlt(void (*pFunct)) {
	functAlt = pFunct;
}

//wait for key event function
void buttonLoopFunction() {
	u_char KeyID;
	buttonSetFunctAlt(&dummyFunction);
	for (;;) {
		if (KbWaitForKeyEvent(50) == KB_OK)
		{
			ButtonPressed();
			KeyID = KbGetKey();
			LogMsg_P(LOG_INFO, PSTR("key pressed: %u"), (unsigned int)KeyID);


			switch ((unsigned int) KeyID)
			{
				case KEY_ESC: functEsc(); break;
				case KEY_OK: functOK(); break;
				case KEY_UP: functUp();	break;
				case KEY_DOWN: functDown(); break;
				case KEY_LEFT: functLeft();	break;
				case KEY_RIGHT:	functRight(); break;
				case KEY_01: funct1(); break;
				case KEY_02: funct2(); break;
				case KEY_03: funct3(); break;
				case KEY_04: funct4(); break;
				case KEY_05: funct5(); break;
				case KEY_ALT: functAlt(); break;
				default: break;
			}
			
			// Key held
			while(KeyID == KbGetKey())
			{
				int altCounter = 0;
				int upCounter = 0;
				int downCounter = 0;

				//LogMsg_P(LOG_INFO, PSTR("Here"));
				// Wait 2 second
				while (KeyID == KEY_ALT)
				{
					NutDelay(100);
					LogMsg_P(LOG_INFO, PSTR("Waiting %i"), altCounter);
					// 20 * 100 = 2 seconds, Alt-key held long enough
					if (altCounter == 20)
					{
						ClearScreen();
						initFirstBoot();
						resetTimeZone();
						// niet nodig want nutreset()
						//doFirstBoot();
						LogMsg_P(LOG_INFO, PSTR("DONE"));
						//return;
						cli();                          // disable all interrupts
						WatchDogStart(0);
						for (;;);
					}

					altCounter ++;
					KeyID = KbGetKey();
				}

				while (KeyID == KEY_UP)
				{
					NutSleep(100);
					if (upCounter >= 10)
					{
						VolumeUp();
						SnoozeTimeUp();
						AlarmTimeUp();
						AlarmTimeUp();
						AlarmTimeUp();
						AlarmTimeUp();
						AlarmTimeUp();
					}
					upCounter ++;
					KeyID = KbGetKey();
				}

				while (KeyID == KEY_DOWN)
				{
					NutSleep(100);
					if (downCounter >= 10)
					{
						VolumeDown();
						SnoozeTimeDown();
						AlarmTimeDown();
						AlarmTimeDown();
						AlarmTimeDown();
						AlarmTimeDown();
						AlarmTimeDown();
					}
					downCounter ++;
					KeyID = KbGetKey();
				}

				if (KeyID == KEY_POWER) 
				{
					if (SettingsGetPowerState() == 0)
					{
						SettingsSetPowerState(1);
						NutDelay(100);
						cli();                          // disable all interrupts
						WatchDogStart(0);
						for (;;);
					}
					else if (SettingsGetPowerState() == 1)
					{
						SettingsSetPowerState(0);
						cli();                          // disable all interrupts
						WatchDogStart(0);
						for (;;);
					}
				}
			}
		}
		NutSleep(1);
	}
}



void buttonResetAll() {
	buttonSetFunctEsc(&dummyFunction);
	buttonSetFunctOK(&dummyFunction);
	buttonSetFunctUp(&dummyFunction);
	buttonSetFunctDown(&dummyFunction);
	buttonSetFunctLeft(&dummyFunction);
	buttonSetFunctRight(&dummyFunction);
	buttonSetFunct1(&dummyFunction);
	buttonSetFunct2(&dummyFunction);
	buttonSetFunct3(&dummyFunction);
	buttonSetFunct4(&dummyFunction);
	buttonSetFunct5(&dummyFunction);
}
