#include <stdio.h>
#include <string.h>

#include <sys/event.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <limits.h>

#include "log.h"

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "alarm.h"
#include "settings.h"

#include "alarm.h"
#include "buttons.h"
#include "ethernet.h"
#include "shoutcast.h"
#include "timecalc.h"
#include "vs10xx.h"

#include "Menu/AlarmButtons.h"
#include "Menu/MainMenu.h"
#include "Menu/TimeStampMenu.h"

#define LOG_MODULE  LOG_MAIN_MODULE

#define   FALSE   0
#define   TRUE    1

#define LOG_MODULE  LOG_MAIN_MODULE

static  HANDLE  alarm1;

// Our timestamp snapshot
static  tm      tm_last;

// Our Alarms
static  tm      tm_alarm1;

static int MyClockComparator(struct _tm *, struct _tm *);

static int MyClockComparator(struct _tm *current, struct _tm *comparedTime)
{
	if (current->tm_hour < comparedTime->tm_hour)
	{
		LogMsg_P(LOG_DEBUG, PSTR("current->tm_hour < comparedTime->tm_hour"));
		return -1;
	}	
	else if (current->tm_hour > comparedTime->tm_hour)
	{
		if ( ((current->tm_hour) - 1) >= comparedTime->tm_hour)
		{
			LogMsg_P(LOG_DEBUG, PSTR("(current->tm_hour) - 1) > comparedTime->tm_hour"));
			return -1; // alarm is voor de volgende dag pas.
		}
		else
		{
			LogMsg_P(LOG_DEBUG, PSTR("not ((current->tm_hour) - 1) > comparedTime->tm_hour)"));
			return 1;
		}
	}
	
	if (current->tm_min < comparedTime->tm_min)
	{
		LogMsg_P(LOG_DEBUG, PSTR("current->tm_min < comparedTime->tm_min"));
		return -1;
	}
	else if (current->tm_min > comparedTime->tm_min) 
	{
		if ( ((current->tm_min) - 1) >= comparedTime->tm_min)
		{	
			LogMsg_P(LOG_DEBUG, PSTR("((current->tm_min) - 1) > comparedTime->tm_min"));
			return -1; // alarm is voor de volgende dag pas.
		}
		else
		{	
			LogMsg_P(LOG_DEBUG, PSTR("not ((current->tm_min) - 1) > comparedTime->tm_min)"));
			return 1;
		}
	}
		
	if (current->tm_sec < comparedTime->tm_sec)
		return -1;
	else if (current->tm_sec > comparedTime->tm_sec)
		return 1;

	return 0;
}

THREAD(activateAlarms, args){
	for (;;)
	{
		X12RtcGetClock(&tm_last);
		LogMsg_P(LOG_DEBUG, PSTR("load time zone: %d"), SettingsLoadTimeZone());
		addToClock(&tm_last, 0, 0, 0, SettingsLoadTimeZone(), 0, 0);
		LogMsg_P(LOG_DEBUG, PSTR("Last time from curtime [%02d:%02d:%02d]"), tm_last.tm_hour, tm_last.tm_min, tm_last.tm_sec);
		LogMsg_P(LOG_DEBUG, PSTR("Last time from alarm [%02d:%02d:%02d]"), tm_alarm1.tm_hour, tm_alarm1.tm_min, tm_alarm1.tm_sec);
		// Check for alarms every minute, 1 == alarm1 is earlier than currentTime
		while (MyClockComparator(&tm_last, &tm_alarm1) == -1)
		{
			X12RtcGetClock(&tm_last);
			addToClock(&tm_last, 0, 0, 0, SettingsLoadTimeZone(), 0, 0);
			LogMsg_P(LOG_DEBUG, PSTR("Alarm: nutsleep 1000"));
			LogMsg_P(LOG_DEBUG, PSTR("Last time from curtime [%02d:%02d:%02d]"), tm_last.tm_hour, tm_last.tm_min, tm_last.tm_sec);
			LogMsg_P(LOG_DEBUG, PSTR("Last time from alarm [%02d:%02d:%02d]"), tm_alarm1.tm_hour, tm_alarm1.tm_min, tm_alarm1.tm_sec);
			NutSleep(10000);
		}

		LogMsg_P(LOG_INFO, PSTR("EVENT"));
		activateAlarm();
		NutThreadDestroy();
		NutThreadExit();
	}

}

THREAD(pendingAlarms, args){
	// Keep checking for alarms
	//int exitCode = 0; // 0 is keep running, 1 is finished, 2 is snooze
	for (;;){
		// Just wait
		NutEventWait(&alarm1, 0);
		// Alarm found
		LogMsg_P(LOG_DEBUG, PSTR("BEEP BEEP BEEP!"));
		ActivateAlarmButtons(&ShowMenuMain);
		SetInMainMenu();
		DisplayCurrentAlarm();
		int* getbeep = GetBeep();
		LogMsg_P(LOG_DEBUG, PSTR("beep debug: %d"), *getbeep);
		if(*getbeep == 1)
		{
			VsPlayerInit();
			VsBeepStart((u_char)2500);
		}
		else if(*getbeep == 0)
		{
			ConnectToStream("145.58.52.144", 80, "/radio1-bb-mp3");
			PlayStream();
		}

		// We are done
		NutThreadDestroy();
		NutThreadExit();

		/*
		// Activate radio for the lols
		ConnectToStream("145.58.52.144", "/3fm-bb-mp3");
		PlayStream();
		*/
	}
}

// Call this to activate Alarm1 || call NutEventPost(HANDLE) directly
void activateAlarm(){
	NutEventPost(&alarm1);
}

void setAlarm(tm *time){
	// Overwrite alarm when already exists
	tm_alarm1 = *time;
	
	NutThreadCreate("pendingAlarms", pendingAlarms, NULL, 256);   // Operation Thread
	NutThreadCreate("activateAlarms", activateAlarms, NULL, 256); // Waiting Thread
}

void ResetAlarm(){
	int snoozetime = SettingsGetSnoozeTime();
	addToClock(&tm_alarm1, 0, 0, 0, 0, snoozetime, 0); // Add 10 seconds
	setAlarm(&tm_alarm1); // snoozeTime
}
