
///
#define LOG_MODULE  LOG_MAIN_MODULE

#include <sys/thread.h>
#include <sys/timer.h>

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>


#include "log.h"
#include "shoutcast.h"

#include <sys/socket.h>
#include "player.h"


#define OK			0
#define NOK			1

FILE *stream;

int ConnectToStream(CONST char addr[], int port, char getStr[])
{
	int result = OK;
	char *data;
	
	TCPSOCKET *sock;
	LogMsg_P(LOG_DEBUG, PSTR("«onnecting to stream..."));
	sock = NutTcpCreateSocket();
	NutSleep(100);
	if( NutTcpConnect(	sock,
						inet_addr(addr), 
						port) )
	{
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect()"));
		
		//exit(1);
		//return NOK;
	}
	stream = _fdopen( (int) sock, "r+b" );
	
	fprintf(stream, "GET %s HTTP/1.0\r\n", getStr);
	fprintf(stream, "Host: %s\r\n", addr);
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Icy-MetaData: 0\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	LogMsg_P(LOG_INFO, PSTR("reached open socket..."));

	// Connection stream debug
	LogMsg_P(LOG_ERR, PSTR("%s"), stream);
	
	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *) malloc(512 * sizeof(char));
	
	while( fgets(data, 512, stream) )
	{
		if( 0 == *data )
			break;
	}
	LogMsg_P(LOG_ERR, PSTR("%s"), data);
	free(data);
	
	return result;
}

int PlayStream(void)
{
	play(stream);

	//int *mp3buf;
	//int rxbytes;
	//int idx = 0;
	//
	//mp3buf = (int *)malloc(sizeof(int) * 2048);
	//
	//rxbytes = fread(mp3buf, 1, 2048, stream );
//
	//for( idx = 0; idx < 2048; idx++ )
	//{
		//printf("%2X ", mp3buf[idx]);
		//if((idx%32)==0)
			//printf("\n");
	//}
	//
	return OK;
}

int StopStream(void)
{
	stop();
	NutSleep(275);
	fclose(stream);	
	
	return OK;
}

