/*! \mainpage SIR firmware documentation
*
*  \section intro Introduction
*  A collection of HTML-files has been generated using the documentation in the source files to
*  allow the developer to browse through the technical documentation of this project.
*  \par
*  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
*  documentation should be done via the source files.
*/




#define LOG_MODULE  LOG_MAIN_MODULE

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <limits.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "Menu/MainMenu.h"
#include "alarm.h"

#include <time.h>
#include "rtc.h"

#include "buttons.h"
#include "Menu/FirstBootMenu.h"
#include "Menu/TimeStampMenu.h"

#include "ethernet.h"
#include "shoutcast.h"
#include "timecalc.h"
#include "settings.h"
#include "ntp.h"

// Clock modes
#define CLOCK_MODE 0
#define MENU_MODE  1
#define ALARM_MODE 2

// Popular timezones
#define US_CENTRAL		-8
#define GREENWICH		0
#define CENTRAL_EUROPE	 1
#define CHINA_COAST		8

/*-------------------------------------------------------------------------*/
/* global variable definitions                                             */
/*-------------------------------------------------------------------------*/
int mode;
/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
static tm datetime;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);

/*-------------------------------------------------------------------------*/
/* Stack check variables placed in .noinit section                         */
/*-------------------------------------------------------------------------*/

/*!
* \addtogroup System
*/



/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/* Threads */
THREAD(ledLightThread, args){
	// Turn on thread
	LcdBackLight(LCD_BACKLIGHT_ON);
	NutSleep((int)args);
	LcdBackLight(LCD_BACKLIGHT_OFF);
	NutThreadExit();
	NutThreadDestroy();
	for(;;) {
		NutSleep(10000);
	}
}

// Not using at the moment
//THREAD(pressEscMenu, args){
	//LcdBackLight(LCD_BACKLIGHT_ON);
	//// Wait for key
	//while (KbWaitForKeyEvent(500) != KB_OK){
//
//}
	//int key = KbGetKey();
	//LogMsg_P(LOG_DEBUG, ("You wrote %i"), key);
	//// We're done here
	//LcdBackLight(LCD_BACKLIGHT_OFF);
	//NutThreadExit();
//}

// Not using on the moment
//THREAD(clockMode, args){
	//for(;;){
		//if (mode == CLOCK_MODE){
			//char timeString[8] = { 0 };
			//getTimeString(&datetime, &timeString);
			//LcdWriteTextOverride(timeString[0]);
		//}
		//NutSleep(750);
	//}
//}

THREAD(buttonsThread, args) {
	NutThreadSetPriority(32);
	// loopt voor altijd door.
	buttonLoopFunction();
	NutThreadExit();
	NutThreadDestroy();
	for(;;) {
		NutSleep(10000);
	}
}


/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
* \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
*
* This routine is automatically called during system
* initialization.
*
* resolution of this Timer ISR is 4,448 msecs
*
* \param *p not used (might be used to pass parms from the ISR)
*/
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
static void SysMainBeatInterrupt(void *p)
{

	/*
	*  scan for valid keys AND check if a MMCard is inserted or removed
	*/
	KbScan();
	CardCheckCard();
}


/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
* \brief Initialise Digital IO
*  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
*
*  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
*  is written to the pin (PORTxn='1')
*/
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
void SysInitIO(void)
{
	/*
	*  Port B:     VS1011, MMC CS/WP, SPI
	*  output:     all, except b3 (SPI Master In)
	*  input:      SPI Master In
	*  pull-up:    none
	*/
	outp(0xF7, DDRB);

	/*
	*  Port C:     Address bus
	*/

	/*
	*  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
	*  output:     Keyboard colums 2 & 3
	*  input:      LCD_data, SDA, SCL (TWI)
	*  pull-up:    LCD_data, SDA & SCL
	*/
	outp(0x0C, DDRD);
	outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

	/*
	*  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
	*  output:     CS Flash, LCD BL/Enable, USB Tx
	*  input:      VS1011 (DREQ), RTL8019, IR
	*  pull-up:    USB Rx
	*/
	outp(0x8E, DDRE);
	outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

	/*
	*  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
	*  output:     LCD RS/RW, LED
	*  input:      Keyboard_Rows, MCC-detect
	*  pull-up:    Keyboard_Rows, MCC-detect
	*  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
	*/

	#ifndef USE_JTAG
	sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
	sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256
	#endif //USE_JTAG

	outp(0x0E, DDRF);
	outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

	/*
	*  Port G:     Keyboard_cols, Bus_control
	*  output:     Keyboard_cols
	*  input:      Bus Control (internal control)
	*  pull-up:    none
	*/
	outp(0x18, DDRG);
}

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
* \brief Starts or stops the 4.44 msec mainbeat of the system
* \param OnOff indicates if the mainbeat needs to start or to stop
*/
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
static void SysControlMainBeat(u_char OnOff)
{
	int nError = 0;

	if (OnOff==ON)
	{
		nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
		if (nError == 0)
		{
			init_8_bit_timer();
		}
	}
	else
	{
		// disable overflow interrupt
		disable_8_bit_timer_ovfl_int();
	}
}

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
* \brief Main entry of the SIR firmware
*
* All the initialisations before entering the for(;;) loop are done BEFORE
* the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
* initialisatons need to be done again when leaving the Setup because new values
* might be current now
*
* \return \b never returns
*/
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
int main(void)
{
	mode = CLOCK_MODE;
	
	/*
	*  First disable the watchdog
	*/
	WatchDogDisable();

	NutDelay(100);

	SysInitIO();
	
	SPIinit();
	
	LedInit();
	
	LcdLowLevelInit();

	Uart0DriverInit();
	Uart0DriverStart();
	LogInit();

	CardInit();

	/*
	* Kroeske: sources in rtc.c en rtc.h
	*/
	//current_timezone = GREENWICH;
	X12Init();
	if (X12RtcGetClock(&datetime) == 0)
	{
		addToClock(&datetime, 0, 0, 0, 0, 0, 0);
		//addToClock(&datetime, 0, 0, 0, 10, 7, 0);
		LogMsg_P(LOG_INFO, PSTR("RTC time [%02d:%02d:%02d]"), datetime.tm_hour, datetime.tm_min, datetime.tm_sec );
	}
	
	if (At45dbInit()==AT45DB041B)
	{
		// ......
	}


	RcInit();
	
	KbInit();

	SysControlMainBeat(ON);             // enable 4.4 msecs hartbeat interrupt

	/*
	* Increase our priority so we can feed the watchdog.
	*/
	NutThreadSetPriority(1);

	/* Enable global interrupts */
	sei();
    
	// Go into clock mode (Display time)
	//NutThreadCreate("clockMode", clockMode, NULL, 256);
	NutThreadCreate("buttonsThread", buttonsThread, NULL, 256);

	if (SettingsGetPowerState() == 0)
	{
		while(SettingsGetPowerState() == 0) 
		{
			NutSleep(10);
		}
	}
	
	LcdBackLight(LCD_BACKLIGHT_ON);

	// init Ethernet interface
	InetInit();

	initFirstBoot();
	doFirstBoot();
	
	tm ntpTime;

	GetTimeFromNTP("95.46.198.21", &ntpTime);
	LogMsg_P(LOG_INFO, PSTR("NTP time [%02d:%02d:%02d]"), ntpTime.tm_hour, ntpTime.tm_min, ntpTime.tm_sec);
	X12RtcSetClock(&ntpTime);

	// TEMP CODE FOR TESTING MENUS
	//ShowMenuMain();

	InitTimeStampMenu(&datetime);

	///* play 3FM */

	//ConnectToStream("145.58.52.144", "/3fm-bb-mp3");
	//ConnectToStream("145.58.52.144", "/radio1-bb-mp3");


	//PlayStream();
	
	// test alarm
	//tm alarmTime;
	//X12RtcGetClock(&alarmTime);
	//addToClock(&alarmTime, 0, 0, 0, 0, 0, 15);
	//setAlarm(&alarmTime);

	//writeLinesFirstLineInCapitals("Hoi", "NerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerdNerd");
	for (;;)
	{
		NutSleep(1000);
		WatchDogRestart();
	}

	return(0);      // never reached, but 'main()' returns a non-void, so.....
}


/* ---------- end of module ------------------------------------------------ */
