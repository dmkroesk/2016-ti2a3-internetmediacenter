#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "Menu/MainMenu.h"
#include "alarm.h"
#include "limits.h"
#include <time.h>

#include "timecalc.h"


// In-/Decrease time in our time structs
void addToClock(struct _tm *tm, int years, int months, int days, int hours, int minutes, int seconds){

	// Seconds
	while (tm->tm_sec + seconds > 59){
		minutes += 1;
		seconds -= 60;
	}
	while (tm->tm_sec + seconds < 0){
		minutes -= 1;
		seconds += 60;
	}		
	tm->tm_sec += seconds;
	// Minutes
	while (tm->tm_min + minutes > 59) {
		hours += 1;
		minutes -= 60;
	}	
	while (tm->tm_min + minutes < 0) {
		hours -= 1;
		minutes += 60;
	}	
	tm->tm_min += minutes;
	// Hours
	while (tm->tm_hour + hours > 23) {
		days += 1;
		hours -= 24;
	}	
	while (tm->tm_hour + hours < 0) {
		days -= 1;
		hours += 24;
	}	
	tm->tm_hour += hours;
	// Days
	while (tm->tm_mday + days > 30) {
		months++;
		days -= 31;
	}	
	while (tm->tm_mday + days < 0) {
		months++;
		days += 31;
	}
	tm->tm_mday += days;
	// Months
	while (tm->tm_mon + months > 11) {
		years++;
		months -= 12;
	}
	while (tm->tm_mon + months < 0) {
		years--;
		months += 12;
	}
	tm->tm_mon += months; 
	// Years
	tm->tm_year += years;
	return;
}
// - 1: current is earlier, 0: equal, 1: comparedTime is earlier
int clockComparator(struct _tm *current, struct _tm *comparedTime){
	if (current->tm_year < comparedTime->tm_year)
		return -1;
	else if (current->tm_year > comparedTime->tm_year)
		return 1;

	if (current->tm_mon < comparedTime->tm_mon)
		return -1;
	else if (current->tm_mon > comparedTime->tm_mon)
		return 1;

	if (current->tm_mday < comparedTime->tm_mday)
		return -1;
	else if (current->tm_mday > comparedTime->tm_mday)
		return 1;

	if (current->tm_hour < comparedTime->tm_hour)
		return -1;
	else if (current->tm_hour > comparedTime->tm_hour)
		return 1;
	
	if (current->tm_min < comparedTime->tm_min)
		return -1;
	else if (current->tm_min> comparedTime->tm_min)
		return 1;

	if (current->tm_sec < comparedTime->tm_sec)
		return -1;
	else if (current->tm_sec > comparedTime->tm_sec)
		return 1;

	return 0;
}
