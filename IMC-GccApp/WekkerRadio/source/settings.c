/*
* settings.c
*
* Created: 26-2-2016 13:29:23
*  Author: Bart
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/event.h>
#include <sys/timer.h>
#include <time.h>

#include "log.h"
#include "keyboard.h"
#include "display.h"
#include "settings.h"
#include "vs10xx.h"
#include "alarm.h"


static tm *datetime;

static int volumeChangeBoolean = 0;
static int volume = 100;

static int alarmTimeChangeBoolean = 0;
static int alarmTimeMinutes = 30;
static int alarmTimeHours = 8;

static int snoozeTimeChangeBoolean = 0;
static int snoozeTime = 5;

static int beep = 1;

#define LOG_MODULE  LOG_MAIN_MODULE

tm* SettingsGetDateTime() 
{
	return datetime;
}

void SettingsSetDateTime(tm *dt) 
{
	datetime = dt;
}

int SettingsGetSnoozeTime()
{
	return snoozeTime;
}
int SettingsGetSnoozeTimeChangeBoolean()
{
	return snoozeTimeChangeBoolean;
}
void SettingsSnoozeTimeChangeBooleanTrue()
{
	snoozeTimeChangeBoolean = 1;
}
void SettingsSnoozeTimeChangeBooleanFalse()
{
	snoozeTimeChangeBoolean = 0;
}

//getters en setters voor alarmTime
int SettingsGetAlarmTimeChangeBoolean()
{
	return alarmTimeChangeBoolean;
}
void SettingsAlarmTimeChangeBooleanTrue()
{
	alarmTimeChangeBoolean = 1;
}
void SettingsAlarmTimeChangeBooleanFalse()
{
	alarmTimeChangeBoolean = 0;
}

int SettingsGetVolumeChangeBoolean()
{
	return volumeChangeBoolean;
}
void SettingsVolumeChangeBooleanTrue()
{
	volumeChangeBoolean = 1;
}
void SettingsVolumeChangeBooleanFalse()
{
	volumeChangeBoolean = 0;
}

//zet alle standaard instellingen terug
void SetDefaultSettings()
{
	volume = 20;
	snoozeTime = 10;
	alarmTimeMinutes = 30;
	alarmTimeHours = 8;
	SaveAlarmTime();
	SettingsSaveTimeZone(0);
}

//volumewaarde veranderen
void VolumeUp()
{
	//controleert of volume aangepast mag worden en niet het max/min volume overschrijdt
	if ( (volume < 100) && (volumeChangeBoolean == 1) )
	{
		//update volume
		volume = volume + 1;

		//update screen
		char showVolume[16];
		ClearScreen();
		writeLineOne("VOLUME");
		sprintf(showVolume, "%d", volume);
		writeLineTwo(showVolume);

		//schrijft volume naar VS chip
		VsSetVolume(100 - volume, 100 - volume);
	}
}
void VolumeDown()
{
	//controleert of volume aangepast mag worden en niet het max/min volume overschrijdt
	if ( (volume > 0) && (volumeChangeBoolean == 1) )
	{
		//update volume
		volume = volume - 1;

		//update screen
		char showVolume[16];
		ClearScreen();
		writeLineOne("VOLUME");
		sprintf(showVolume, "%d", volume);
		writeLineTwo(showVolume);

		//schrijft volume naar VS chip
		VsSetVolume(100 - volume, 100 - volume);
	}
}

//alarmTime veranderen
void AlarmTimeUp()
{
	//controleert of alarmTime veranderd mag worden
	if (alarmTimeChangeBoolean == 1)
	{	
		//update alarmTime
		alarmTimeMinutes ++;
		if (alarmTimeMinutes == 60)
		{
			alarmTimeMinutes = 0;
			alarmTimeHours ++;
			if (alarmTimeMinutes == 24)
			{
				alarmTimeHours = 0;
			}
		}
	
		//update screen
		char showAlarmTime[16];
		ClearScreen();
		writeLineOne("SET TIME");
		sprintf(showAlarmTime, "%02d:%02d", alarmTimeHours, alarmTimeMinutes);
		writeLineTwo(showAlarmTime);
	}
}
void AlarmTimeDown()
{
	//controleert of alarmTime veranderd mag worden
	if (alarmTimeChangeBoolean == 1)
	{
		//update alarmTime
		alarmTimeMinutes --;
		if (alarmTimeMinutes == -1)
		{
			alarmTimeMinutes = 59;
			alarmTimeHours --;
			if (alarmTimeMinutes == -1)
			{
				alarmTimeHours = 23;
			}
		}
		
		//update screen
		char showAlarmTime[16];
		ClearScreen();
		writeLineOne("SET TIME");
		sprintf(showAlarmTime, "%02d:%02d", alarmTimeHours, alarmTimeMinutes);
		writeLineTwo(showAlarmTime);
	}
}

void SnoozeTimeUp()
{
	if (SettingsGetSnoozeTimeChangeBoolean() == 1 && snoozeTime < 180)
	{
		//update snoozetime
		snoozeTime ++;

		//update screen
		char showSnoozeTime[16];
		ClearScreen();
		writeLineOne("SNOOZE TIME");
		sprintf(showSnoozeTime, "%d", snoozeTime);
		strcat(showSnoozeTime, " minutes");
		writeLineTwo(showSnoozeTime);
	}
}
void SnoozeTimeDown()
{
	if (SettingsGetSnoozeTimeChangeBoolean() == 1 && snoozeTime > 1)
	{
		//update snoozetime
		snoozeTime --;

		//update screen
		char showSnoozeTime[16];
		ClearScreen();
		writeLineOne("SNOOZE TIME");
		sprintf(showSnoozeTime, "%d", snoozeTime);
		strcat(showSnoozeTime, " minutes");
		writeLineTwo(showSnoozeTime);
	}
}

void SettingsSaveTimeZone(int _utc) {
	eeprom_write_byte( (uint8_t *)10, _utc);
}
int SettingsLoadTimeZone() {
	return eeprom_read_byte( (uint8_t *)10 );
}

void SetBeep(int _beep)
{
	beep = _beep;
}
int* GetBeep()
{
	return &beep;
}

//opslaan en laden van alarmTime
// 1 staat voor uren, 0 staat voor minuten
int LoadAlarmTime(int timeSwitch)
{
	if(timeSwitch == 1)
	{
		return eeprom_read_byte( (uint8_t *)15 );
	}

	else if(timeSwitch == 0)
	{
		return eeprom_read_byte( (uint8_t *)20);
	}
	else
	{
		return 0;
	}
}
void SaveAlarmTime()
{
	tm alarmTime;
	X12RtcGetClock(&alarmTime);
	alarmTime.tm_min = alarmTimeMinutes;
	alarmTime.tm_hour = alarmTimeHours;
	setAlarm(&alarmTime);
	eeprom_write_byte( (uint8_t *)15, alarmTimeHours);
	eeprom_write_byte( (uint8_t *)20, alarmTimeMinutes);
	LogMsg_P(LOG_DEBUG, PSTR("saveAlarmTime(): %d %d"), alarmTimeHours, alarmTimeMinutes);
}
//laadt de alarmtijd uit het geheugen als deze moet worden weergegeven
void ShowAlarmTime()
{
	//checkt of alarmTijd is opgeslagen in het geheugen
	if(LoadAlarmTime(0) != 0 && LoadAlarmTime(0) != 0)
	{
		alarmTimeMinutes = LoadAlarmTime(0);
		alarmTimeHours = LoadAlarmTime(1);
	}
}

void SettingsSaveSettings()
{	
	//volume, snooze, minutes, hours, timezone
	eeprom_write_byte( (uint8_t *)30, volume);
	eeprom_write_byte( (uint8_t *)35, snoozeTime);
	eeprom_write_byte( (uint8_t *)40, alarmTimeMinutes);
	eeprom_write_byte( (uint8_t *)45, alarmTimeHours);
	eeprom_write_byte( (uint8_t *)50, SettingsLoadTimeZone());
}
void SettingsLoadSettings()
{
	//volume, snooze, minutes, hours, timezone
	int randomMemory = eeprom_read_byte( (uint8_t *)30);

	if (randomMemory >= 0 && randomMemory <= 100)
	{
		volume = randomMemory;
		randomMemory = eeprom_read_byte( (uint8_t *)35);
	}
	
	if (randomMemory >= 0 && randomMemory <= 180)
	{
		snoozeTime = randomMemory;
		randomMemory = eeprom_read_byte( (uint8_t *)40);
	}
	
	if (randomMemory >= 0 && randomMemory <= 59)
	{
		alarmTimeMinutes = randomMemory;
		randomMemory = eeprom_read_byte( (uint8_t *)45);
	}
	
	if (randomMemory >= 0 && randomMemory <= 23)
	{
		alarmTimeHours = randomMemory;
		randomMemory = eeprom_read_byte( (uint8_t *)50);
	}
	
	if (randomMemory >= -12 && randomMemory <= 12)
	{
		SettingsSaveTimeZone(randomMemory);
	}
}

int SettingsGetVolume()
{
	return volume;
}

void SettingsSetVolume(int _volume)
{
	volume = _volume;
}

int SettingsGetPowerState()
{
	int state = eeprom_read_byte( (uint8_t *)25 );
	if (state != 0)
		return 1;
	else
		return 0;
}

void SettingsSetPowerState(int state)
{
	eeprom_write_byte( (uint8_t *)25, state);
}
