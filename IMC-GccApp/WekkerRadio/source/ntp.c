/*
* ntp.c
*
* Created: 10-3-2016 16:17:31
*  Author: Bart
*/

// AVR includes
#include <stdio.h>
#include <stdlib.h>

// NUT O/S includes
#include <pro/sntp.h>
#include <arpa/inet.h>
#include <time.h>

#include "ntp.h"
#include "settings.h"
#include "rtc.h"

//int gmtime_r(CONST time_t * timer, tm * theTime);

int GetTimeFromNTP(char address[], tm *utctime) {
	u_long addr = inet_addr(address);
	time_t tmpTime = 0;
	int i = NutSNTPGetTime(&addr, &tmpTime);
	gmtime_r(&tmpTime, utctime);
	return i;
}

void SetTimeZone(int zoneCode){
	if (zoneCode == SettingsLoadTimeZone())
	return; // Same zone, no recal needed

	addToClock(SettingsGetDateTime(), 0, 0, 0, (zoneCode - SettingsLoadTimeZone()), 0, 0);
	SettingsSaveTimeZone(zoneCode);
}

