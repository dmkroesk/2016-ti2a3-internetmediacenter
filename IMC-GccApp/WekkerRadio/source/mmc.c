/* ========================================================================
* [PROJECT]    SIR100
* [MODULE]     MMC driver
* [TITLE]      Media Card driver
* [FILE]       mmc.c
* [VSN]        1.0
* [CREATED]    02 october 2006
* [LASTCHNGD]  20 may 2007
* [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
* [PURPOSE]    routines and API to support MMC-application
* ======================================================================== */

#define LOG_MODULE  LOG_MMC_MODULE

#include <string.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

#include <sys/event.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/device.h>
#include <sys/bankmem.h>
#include <sys/heap.h>

//#pragma text:appcode

#include "system.h"
#include "mmc.h"
#include "portio.h"
#include "vs10xx.h"
#include "display.h"
#include "log.h"
#include "settings.h"
#include "fat.h"
#include "mmcdrv.h"
#include "led.h"
#include "util.h"
#include "keyboard.h"
#include "player.h"
#include "shoutcast.h"

#ifdef DEBUG
//#define MMC__DEBUG
#endif /* #ifdef DEBUG */
/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/

#define CARD_PRESENT_COUNTER_OK         30
#define CARD_NOT_PRESENT_COUNTER_OK     20

#define MMC_BUFSIZE                     12288
#define MAX_FILENAMESIZE                25

#define NO_SONG                         0

#define TRACK_START_LENGTH              5000        // add these amount of zero's in between tracks

#define ALGO_INTERVALTIME               3
#define CARD_MAX_BADAUDIO               2

/*--------------------------------------------------------------------------*/
/*  Type declarations                                                       */
/*--------------------------------------------------------------------------*/
/*!\brief State of this module */
typedef enum T_STREAMER_STATE
{
	STATE_IDLE = 0,                 /* We are idle */
	STATE_STARTING,                 /* We're connecting to a new stream */
	STATE_PLAYING,                  /* We're playing a stream */
	STATE_STOPPING                  /* We're stopping playback */
} TMMCState;

/*!\brief Statemachine  for feeding bytes to the audiobufer */
typedef enum T_FEEDING_STATE
{
	FEEDING_IDLE = 0,               /* nothing to do */
	FEEDING_WAIT_PLAY,              /* filling audiobuffer */
	FEEDING_BEGIN,                  /* start condition: nothing done */
	FEEDING_ARCHIVE,                /* bitrate adaptation not required */
} TFeedingState;

/*!\brief Statemachine for card-detection */
typedef enum T_CARD_STATE
{
	CARD_IDLE,                      /* nothing to do */
	CARD_PRESENT,                   /* card seen at least one time */
	CARD_VALID,                     /* card seen at least <valid> times */
	CARD_NOT_PRESENT                /* card not seen at least (valid> times */
}TCardState;

/*!\brief structure to hold song-information  */
struct TPLAYLISTENTRY
{
	struct TPLAYLISTENTRY *pNextEntry;
	char szFileName[MAX_FILENAMESIZE];
	char szTitle[DISPLAY_SIZE];
	int uLength;
};

typedef struct TPLAYLISTENTRY TPlayListEntry;

/*!\brief Strucutre to hold playlist information */
typedef struct TPLAYLIST
{
	int fid;
	u_int uNrofSongs;
	u_int uCurrentSong;
	char* pbyFileBuf;
	TPlayListEntry *pFirstEntry;
	TPlayListEntry *pCurrentEntry;
}TPlayList;


/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
static u_char CardPresentFlag;
static u_char ValidateCounter;
//static u_char g_StreamDecoded=0;
static u_char *DecriptKey=NULL;     // ptr to allocated RAM in which the 4096 byte Key is stored

/*!\brief MMC player state */
static TMMCState        g_tState;

/*!\brief Current file */
static int              g_fd;

/*!\brief stateID when feeding data to the audiobuffer */
static TFeedingState CardFeedState;

/*!\brief state-variable for Card-statemachine */
static TCardState CardState;

/*!\brief Status of this module */
static TError g_tStatus;

/*!\brief We only start playing when bufsize is larger than this */
static u_long StartPlayBufSize;

/*!\brief Time counter. If 0 we start to play the stream. */
static u_char g_byWaitTimer;
static u_char g_byPlayerIntervalTicks;

/*!\brief container for Playlist information */
static TPlayList g_PlayList;

/*!\brief this list is now being played */
//static u_char g_CurrentPlayList;

/*!\brief the number of playlists that was counted by the init-routine */
static u_char g_NrofPlayLists;

/*!\brief pointer to allocated RAM that is holding a copy of the Playlist */
//static char* g_pbyFilePos;


static char szCardState[13];
/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static TError CardProcessAudio(void);
//static TError CardOpenPlayList(u_char);
//static char*  CardReadPlayListTag(char*);
//static TError CardGetNextSongFromPlayList(TPlayListEntry*);
static char* CardGetState(TMMCState State);


static FILE *mystream;


/*!
* \addtogroup Card
*/

/*@{*/

	/*-------------------------------------------------------------------------*/
	/*                         start of code                                   */
	/*-------------------------------------------------------------------------*/

	/*!
	* \brief check if MM-Card is inserted or removed.
	*
	* \Note: this routine is called from an ISR !
	*
	*/
	u_char CardCheckCard(void)
	{
		u_char RetValue=CARD_NO_CHANGE;

		switch (CardState)
		{
			case CARD_IDLE:
			{
				if (bit_is_clear(MMC_IN_READ, MMC_CDETECT))
				{
					ValidateCounter=1;
					CardState = CARD_PRESENT;
				}
			}
			break;
			case CARD_PRESENT:
			{
				if (bit_is_clear(MMC_IN_READ, MMC_CDETECT))
				{
					if (++ValidateCounter==CARD_PRESENT_COUNTER_OK)
					{
						CardPresentFlag=CARD_IS_PRESENT;
						CardState=CARD_VALID;
						RetValue=CARD_IS_PRESENT;
					}
				}
				else
				{
					CardState=CARD_IDLE;                  // false alarm,start over again
				}
			}
			break;
			case CARD_VALID:
			{
				if (bit_is_set(MMC_IN_READ, MMC_CDETECT))
				{
					ValidateCounter=1;
					CardState=CARD_NOT_PRESENT;         // Card removed
				}
			}
			break;
			case CARD_NOT_PRESENT:
			{
				if (++ValidateCounter==CARD_NOT_PRESENT_COUNTER_OK)
				{
					CardPresentFlag=CARD_IS_NOT_PRESENT;
					CardState=CARD_IDLE;
					RetValue=CARD_IS_NOT_PRESENT;
				}
			}
			break;
		}
		return(RetValue);
	}

	/*!
	* \brief return status of "Card is Present"
	*
	*/
	u_char CardCheckPresent()
	{
		return(CardPresentFlag);
	}

	/*!
	* \brief initialise the card by reading card contents (.pls files)
	*
	* We initialse the card by registering the card and the filesystem
	* that is on the card.
	*
	* Then we start checking if a number of playlists are
	* present on the card. The names of these playlists are hardcoded
	* (1.pls, 2.pls, to 20.pls). We 'search' the card for these list
	* of playlists by trying to open them. If succesfull, we read the
	* number of songs present (int) in that list
	* Finally we update some administration (global) variables
	*
	*/
	int CardInitCard()
	{
		int iResult=-1;
		int fid;        // current file descriptor
		char szFileName[10];
		u_char i, ief;


		/*
		* Register our device for the file system (if not done already.....)
		*/
		if (NutDeviceLookup(devFAT.dev_name) == 0)
		{
			ief = VsPlayerInterrupts(0);
			if ((iResult=NutRegisterDevice(&devFAT, FAT_MODE_MMC, 0)) == 0)
			{
				iResult=NutRegisterDevice(&devFATMMC0, FAT_MODE_MMC, 0);
			}
			VsPlayerInterrupts(ief);
		}
		else
		{
			NUTDEVICE * dev;

			/*
			*  we must call 'FatInit' here to initialise and mount the filesystem (again)
			*/

			FATRelease();
			ief = VsPlayerInterrupts(0);
			dev=&devFAT;
			if (dev->dev_init == 0 || (*dev->dev_init)(dev) == 0)
			{
				dev=&devFATMMC0;
				if (dev->dev_init == 0 || (*dev->dev_init)(dev) == 0)
				{
					iResult=0;
				}
			}
			VsPlayerInterrupts(ief);
		}

		if (iResult==0)
		{
			LogMsg_P(LOG_INFO, PSTR("Card mounted"));
			/*
			*  try to open the playlists. If an error is returned, we assume the
			*  playlist does not exist and we do not check any further lists
			*/
			
			

			for (i=1; i < 10; ++i)
			{
				// compose name to open
				sprintf_P(szFileName, PSTR("FM0:test%i.mp3"), i);
				LogMsg_P(LOG_DEBUG, PSTR("Filename Playlist: %s"), &szFileName);
				if ((fid = _open(szFileName, _O_RDONLY)) != -1)
				{
					//void *buff = NULL;
					//int byteRead = _read(fid,buff,512);
					//char* hoiBytes = (char*)buff;
					//LogMsg_P(LOG_DEBUG, PSTR("Gevonden: %s"), &hoiBytes);
					//LogMsg_P(LOG_DEBUG, PSTR("Aantal bytes: %d"), byteRead);
					
					_close(fid);
				}
				else
				{
					g_NrofPlayLists=i-1;
					LogMsg_P(LOG_INFO, PSTR("Found %d Playlists on the Card"), i-1);
					break;
				}
			}
		}
		else
		{
			LogMsg_P(LOG_ERR, PSTR("Error initialising File system and Card-driver"));
		}

		return(iResult);
	}

	/*!
	* \brief The CardPresent thread.
	*
	* execute code when card is inserted or redrawn
	*
	* \param   -
	*
	* \return  -
	*/
	THREAD(CardPresent, pArg)
	{
		static u_char OldCardStatus;

		OldCardStatus=CardPresentFlag;

		for (;;)
		{
			if ((CardPresentFlag==CARD_IS_PRESENT) && (OldCardStatus==CARD_IS_NOT_PRESENT))
			{
				LogMsg_P(LOG_INFO, PSTR("Card inserted"));
				if (CardInitCard()==0)
				{
					KbInjectKey(KEY_MMC_IN);
				}
				OldCardStatus=CardPresentFlag;
			}
			else if ((CardPresentFlag==CARD_IS_NOT_PRESENT) && (OldCardStatus==CARD_IS_PRESENT))
			{
				LogMsg_P(LOG_INFO, PSTR("Card removed"));
				CardClose();
				OldCardStatus=CardPresentFlag;
			}
			else
			{
				NutSleep(500);
			}
		}
	}

	/*!
	* \brief The main card thread.
	*
	* If we're not playing a song, sleep
	* to give other threads execution time.
	*
	* \param   -
	*
	* \return  -
	*/
	THREAD(CardPlayer, pArg)
	{
		TError tError = OK;             /* Error status */
		for (;;)
		{
			if (g_tState != STATE_IDLE)
			{
				/*
				* We still have work to do
				*/
				tError=CardProcessAudio();
				g_tStatus=tError;
			}
			else
			{
				/*
				* Sleep if we have nothing to do
				*/
				NutSleep(1000);
			}
		}
	}



	/*!
	* \brief Play audio from a Card
	*
	* \Note this is the MMC equivalent of 'ProcessAudio' in 'streamer.c'
	*/
	static TError CardProcessAudio(void)
	{
		TError tError = OK;
		u_char ief = 0;
		size_t dwBufFree = 0;
		u_long dwTotUsed = 0;
		u_char byBadAudioCount = 0;
		u_char bNewSong = 0;
		u_char *pbyBuf = NULL;
		char szFileName[MAX_FILENAMESIZE + 4];
		TError tOpenError;

		if (!g_fd)
		{
			return(CARD_CREATE_STREAM);
		}

		/*
		* Only reset our buffers if we are starting a fresh stream
		*/
		if (g_tState == STATE_STARTING)
		{
			/*
			* Reset the buffer
			*/
			ief = VsPlayerInterrupts(0);

			NutSegBufReset();
			NutSegBufInit(MMC_BUFSIZE);

			VsPlayerInterrupts(ief);

			CardFeedState = FEEDING_BEGIN;
			g_tStatus = CARD_BUFFERING;
		}

		/*
		* We have a valid file descriptor and ready to play some music
		*/
		if (g_tState != STATE_STOPPING)
		{
			g_tState = STATE_PLAYING;

			/*
			* Keep reading from the card as long as there is data
			* The VS-ISR (DREQ) will feed it into the MP3 device (using the
			* audio buffer) as a interrupt-backgroud process
			*/
			while (tError == OK)
			{
				/*
				* Query number of byte available in the current bank of the MP3 buffer.
				*/
				ief = VsPlayerInterrupts(0);
				pbyBuf = (u_char *)NutSegBufWriteRequest(&dwBufFree);
				VsPlayerInterrupts(ief);

				/*
				* First handle the input (= file on card) side
				*/
				if (dwBufFree > 0)
				{
					int nReceived = 0;
					/*
					*  while reading from the card, don't allow DREQ-interrupts
					*/
					ief = VsPlayerInterrupts(0);
					if ((CardFeedState==FEEDING_ARCHIVE) && (dwBufFree>3000))
					{
						// while playing, be modest when reading parts of the file
						nReceived = _read(g_fd, pbyBuf, 3000);
					}
					else
					{
						// not yet playing, so take the chunks as large as possible
						nReceived = _read(g_fd, pbyBuf, dwBufFree);
					}
					VsPlayerInterrupts(ief);

					// regular check for card
					if (CardPresentFlag==CARD_IS_NOT_PRESENT)
					{
						break;
					}

					if (nReceived > 0)
					{
						/*
						*  feed to the Audio-buffer from where it is feed to the VS-chip (DREQ-ISR)
						*  In case a new song is started, first write que-number to the stream
						*/
						//
						if (bNewSong)
						{
							bNewSong=0;
						}
						ief = VsPlayerInterrupts(0);
						pbyBuf = (u_char *)NutSegBufWriteCommit(nReceived);
						VsPlayerInterrupts(ief);
						dwBufFree -= (u_short)nReceived;
					}
					else
					{
						if (CardFeedState!=FEEDING_ARCHIVE)
						{
							// small file, force start of playing
							LogMsg_P(LOG_DEBUG, PSTR("small file, let's start"));
							StartPlayBufSize = 4096;
						}
						else
						{
							do
							{
								// close previous file first
								if (g_fd>=0)
								{
									_close(g_fd);
								}

								if (g_PlayList.pCurrentEntry->pNextEntry==NULL)
								{
									// start at the first entry... (loop playlist)
									g_PlayList.pCurrentEntry=g_PlayList.pFirstEntry;
									g_PlayList.uCurrentSong=1;
								}
								else
								{
									g_PlayList.pCurrentEntry=g_PlayList.pCurrentEntry->pNextEntry;
									g_PlayList.uCurrentSong++;
								}

								// play this song (will set the g_tState to 'STATE_STARTING')
								// first add path-prefix
								strcpy_P(szFileName, PSTR("FM0:"));
								strcat(szFileName, g_PlayList.pCurrentEntry->szFileName);

								tOpenError=OK;
								/*
								*  not sure why we have to disable the DREQ-ISR here, but if we don't
								*  the audio gets distorted when a file is not present ('Error opening...')
								*/
								ief = VsPlayerInterrupts(0);
								if ((g_fd = _open(szFileName, _O_RDONLY | _O_BINARY)) == -1)
								{
									LogMsg_P(LOG_ERR, PSTR("Error opening: %s (skipping)"), szFileName);
									tOpenError=CARD_NO_SONG;
								}
								else
								{
									LogMsg_P(LOG_INFO, PSTR("reading: %s "), g_PlayList.pCurrentEntry->szFileName);
									bNewSong=1;
								}
								VsPlayerInterrupts(ief);
							}while ((tOpenError!=OK) && (g_tState != STATE_STOPPING));
						}
					}
				}

				// regular check for card
				if (CardPresentFlag==CARD_IS_NOT_PRESENT)
				{
					break;
				}

				/*
				* Now handle the output (= audio chip) side
				*/
				ief = VsPlayerInterrupts(0);
				dwTotUsed = NutSegBufUsed();
				VsPlayerInterrupts(ief);

				switch (CardFeedState)
				{
					case FEEDING_BEGIN:
					{
						VsPlayerReset(0);
					}
					/* No break; fallthrough */

					case FEEDING_IDLE:
					{
						StartPlayBufSize = 384000/2;
						g_byWaitTimer = 10;
						CardFeedState = FEEDING_WAIT_PLAY;
						g_tStatus = CARD_BUFFERING;
						LedControl(LED_FLASH_ON);
					}
					break;

					case FEEDING_WAIT_PLAY:
					{
						if (((dwTotUsed > StartPlayBufSize) || (g_byWaitTimer == 0)) && (dwTotUsed > 0))
						{
							g_tStatus = CARD_PLAYING;
							LogMsg_P(LOG_DEBUG, PSTR("Start Playing, Buffersize: %lu"), dwTotUsed);
							LedControl(LED_POWER_OFF);
							g_byPlayerIntervalTicks = 0;
							(void)VsPlayerKick();
							CardFeedState = FEEDING_ARCHIVE;
						}
						else
						{
							LogMsg_P(LOG_DEBUG, PSTR("Filling - Buf: %lu Freemem: %u"), dwTotUsed, NutHeapAvailable());
						}
					}
					break;

					case FEEDING_ARCHIVE:
					{
						/*
						*  nothing to do here, only check if the VS-chip is running and the buffer is not empty
						*/

						// check VS-chip
						if (VsGetStatus()!=VS_STATUS_RUNNING)
						{
							LogMsg_P(LOG_INFO, PSTR("Playing was stopped. Reason: %d"), VsGetStatus());
							// in case there is still audio in the buffer, keep feeding...
							if (dwTotUsed != 0)
							{
								CardFeedState = FEEDING_WAIT_PLAY;
								g_byWaitTimer = 10;
							}
						}
						else
						{
							u_short unHdat = 0;
							if (g_byPlayerIntervalTicks >= ALGO_INTERVALTIME)
							{
								g_byPlayerIntervalTicks = 0;

								unHdat = VsStreamValid();
								if (unHdat == 0)
								{
									LogMsg_P(LOG_INFO, PSTR("Invalid audio %d"), (int)byBadAudioCount);
									if (++byBadAudioCount >= CARD_MAX_BADAUDIO)
									{
										tError = CARD_BADAUDIO;
									}
								}
								else
								{
									byBadAudioCount = 0;
								}

								LogMsg_P(LOG_DEBUG, PSTR("Playing - Buf: %lu used Freemem: %u"), dwTotUsed, NutHeapAvailable());
							}

						}
						// check buffer
						if (dwTotUsed == 0)
						{
							/* End of buffer reached because of end-of-file approached */
							LogMsg_P(LOG_INFO, PSTR("End of File"));
							CardFeedState = FEEDING_IDLE;

							tError = CARD_BUFFEREMPTY;
							_close(g_fd);
						}
					}
					break;

					default:
					{
						CardFeedState = FEEDING_IDLE;
					}
					break;
				}


				// regular check for card
				if (CardPresentFlag==CARD_IS_NOT_PRESENT)
				{
					break;
				}


				NutSleep(100);       // allow other treads to run

				/*
				* Break out of while loop if we need to
				*/
				if (g_tState == STATE_STOPPING)
				{
					CardFeedState = FEEDING_IDLE;
					VsPlayerStop();
					_close(g_fd);

					g_tState=STATE_IDLE;    // this will let the main thread goto sleep
					tError = USER_ABORT;    // break out of while-loop
					LogMsg_P(LOG_INFO, PSTR("Stop request"));
				}
			}  // while (tError==OK)

			if (CardPresentFlag==CARD_IS_NOT_PRESENT)
			{
				VsPlayerStop();
				g_tState=STATE_IDLE;
				tError = CARD_NO_CARD;
			}
		}

		LedControl(LED_POWER_OFF);

		return(tError);
	}

	///*
	//* Play MP3 file from local file system.
	//*
	//* \param path Pathname of the MP3 file to play.
	//*
	//* \return 0 on success, -1 if opening the file failed.
	//*/
	//TError CardPlayMp3File(char *path)
	//{
		//char szFileName[MAX_FILENAMESIZE + 4];
//
		//if (CardCheckPresent() == CARD_IS_NOT_PRESENT)
		//{
			//LogMsg_P(LOG_ERR, PSTR("No Card"));
			//return(CARD_NO_CARD);
		//}
//
//
		///*
		//* Check if our device and filesystem were registered...
		//*/
		//if (NutDeviceLookup(devFAT.dev_name) == 0)
		//{
			//LogMsg_P(LOG_ERR, PSTR("Card and Filesystem not registerd"));
			//return(CARD_NOT_REGISTERED);
		//}
//
		///*
		//* Open the MP3 file, but first check if we need to decode the stream
		//*/
		//LogMsg_P(LOG_INFO, PSTR("Play %s: "), path);
//
		//// first add path-prefix
		//strcpy_P(szFileName, PSTR("FM0:"));
		//strcat(szFileName, g_PlayList.pCurrentEntry->szFileName);
//
		//if ((g_fd = _open(szFileName, _O_RDONLY | _O_BINARY)) == -1)
		//{
			//LogMsg_P(LOG_ERR, PSTR("Error opening: %s "), szFileName);
			//return(CARD_NO_SONG);
		//}
//
		///*
		//* We're not playing: start
		//*/
		//if (g_tState == STATE_IDLE)
		//{
			//// Start the player
			//g_tState = STATE_STARTING;
		//}
//
		//return(OK);
	//}

	/*!
	* \brief stop playing the current song
	*
	*/
	void CardStopMp3File()
	{
		// Stop the player
		if (g_tState != STATE_IDLE)
		{
			LogMsg_P(LOG_INFO, PSTR("Stop current song %d"), g_PlayList.uCurrentSong);
			g_tState = STATE_STOPPING;
		}
	}


	/*!
	* \brief get systemticks to be able to due regular checks
	*
	*/
	void CardUpdateTicks(void)
	{
		if (g_byWaitTimer > 0)
		{
			g_byWaitTimer--;
		}

		g_byPlayerIntervalTicks++;
	}



	/*!
	* \brief Get the current song number.
	*
	* \param   -
	*
	* \return  The number.
	*/
	u_int CardGetCurrentSong(void)
	{
		return(g_PlayList.uCurrentSong);
	}

	/*!
	* \brief Get the current song name.
	*
	* \param   call by reference with u_int container
	*
	* \return  pointer to the string containing the name
	*/
	char* CardGetCurrentSongName(unsigned int *punLength)
	{
		char* szTemp=NULL;

		// hack untill we can sync with VS-chip
		*punLength=0;
		/*
		if (g_PlayList.pCurrentEntry)
		{
		szTemp=g_PlayList.pCurrentEntry->szTitle;
		*punLength=strlen(szTemp);
		}
		else
		{
		*punLength=0;
		}
		*/
		return(szTemp);
	}

	/*!
	* \brief returns the number of playlists available on the card.
	*  Returns NO_PLAYLIST in case no playlists are available.
	*
	*/
	u_char CardGetNumberOfPlayLists(void)
	{
		return(g_NrofPlayLists);
	}

	/*!
	* \brief return global variable that indicates the status of this module
	*
	*/
	TError CardStatus(void)
	{
		return(g_tStatus);
	}

	/*!
	* \brief Stop playing.
	*
	* \param   -
	*
	* \return  -
	*/
	void CardClose(void)
	{
		TPlayListEntry *pTemp, *pNextTemp;

		LogMsg_P(LOG_INFO, PSTR("Close: state [%s]"), CardGetState(g_tState));

		// free all elements of our linked list
		pTemp=g_PlayList.pFirstEntry;
		while (pTemp)
		{
			pNextTemp=pTemp->pNextEntry;
			MyFree(pTemp);
			pTemp=pNextTemp;
		}
		// free our databuffer with the 4096 byte decription key
		if (DecriptKey!=NULL)
		{
			MyFree(DecriptKey);
			DecriptKey=NULL;
		}

		CardStopMp3File();
		g_PlayList.pFirstEntry=NULL;
		g_PlayList.pCurrentEntry=NULL;
		g_PlayList.uCurrentSong=NO_SONG;
		/*
		* Wait for the request to be handled
		*/
		while (g_tState != STATE_IDLE)
		{
			NutSleep(100);
			LogMsg_P(LOG_INFO, PSTR("waiting for idle"));
		}
	}

	///*!
	//* \brief Open the selected playlist and start with the first song found
	//* \Note that 'byPlalist; starts at '0' while the filenames will start at '1'
	//*
	//*/
	//TError CardOpen(u_char byPlayList)
	//{
		//TError tStatus=OK;
		//TPlayListEntry* pTempEntry;
//
//
//
		//if (byPlayList>g_NrofPlayLists)
		//{
			//return(CARD_BADPLAYLIST);
		//}
//
		///*
		//*  check if we were playing a song, if so, kill current song first
		//*/
		//if (g_tState != STATE_IDLE)
		//{
			//CardClose();
		//}
//
		///*
		//*  init playlist file administration
		//*/
		//g_PlayList.fid=0;
		//g_PlayList.uNrofSongs=0;
		//g_PlayList.uCurrentSong=NO_SONG;
		//g_PlayList.pbyFileBuf=NULL;
		//g_PlayList.pFirstEntry=NULL;
		//g_PlayList.pCurrentEntry=NULL;
		//g_StreamDecoded=0;
//
		///*
		//*  open the playlist and check if it is valid
		//*  Note that the global variable 'g_PlayList.uNrofSongs will
		//*  get updated in this routine
		//*/
		////
		//tStatus=CardOpenPlayList(byPlayList);
//
		//if (tStatus==OK)
		//{
			//g_CurrentPlayList=byPlayList;
			//g_PlayList.uNrofSongs=0; // reset because we are going to count again...
			///*
			//*  create the linked list that holds the info of the songs in this playlist
			//*/
			//do
			//{
				//// create (next) element in linked list
				//pTempEntry=(TPlayListEntry*)MyMalloc(sizeof(TPlayListEntry));
				//if (pTempEntry)
				//{
					//if ((tStatus=CardGetNextSongFromPlayList(pTempEntry))==OK)
					//{
						//++g_PlayList.uNrofSongs;
						//pTempEntry->pNextEntry=NULL;
//
						//if (g_PlayList.pFirstEntry==NULL)
						//{
							//// just created the first element?
							//g_PlayList.pFirstEntry=pTempEntry;
							//g_PlayList.pCurrentEntry=pTempEntry;
						//}
						//else
						//{
							//// let previous found element point to this new one..
							//g_PlayList.pCurrentEntry->pNextEntry=pTempEntry;
							//// and make the Current Entry head of our list again
							//g_PlayList.pCurrentEntry=pTempEntry;
						//}
					//}
				//}
				//else
				//{
					//LogMsg_P(LOG_ERR, PSTR("Unable to create next entry"));
				//}
			//}while (tStatus==OK);
//
			//LogMsg_P(LOG_INFO, PSTR("SIR counted %d songs in this playlist"), g_PlayList.uNrofSongs);
//
			//// now set current entry back to the first one and try to play it
			//g_PlayList.pCurrentEntry=g_PlayList.pFirstEntry;
//
			//if (g_PlayList.pCurrentEntry->szFileName[0] !='\0')
			//{
				//g_PlayList.uCurrentSong=1;
				//// play this song
				//tStatus=CardPlayMp3File(g_PlayList.pCurrentEntry->szFileName);
//
				//// catch non-existing file exception
				//if (tStatus==CARD_NO_SONG)
				//{
					//LogMsg_P(LOG_DEBUG, PSTR("skipping %s"), g_PlayList.pCurrentEntry->szFileName);
//
					///*
					//*  get next entry from the playlist, skip non existing songs
					//*/
					//tStatus=OK;
					//do
					//{
						//if (g_PlayList.pCurrentEntry->pNextEntry==NULL)
						//{
							//// start at the first entry... (loop playlist)
							//g_PlayList.pCurrentEntry=g_PlayList.pFirstEntry;
							//g_PlayList.uCurrentSong=1;
						//}
						//else
						//{
							//g_PlayList.pCurrentEntry=g_PlayList.pCurrentEntry->pNextEntry;
							//g_PlayList.uCurrentSong++;
						//}
//
						//// play this song (will set the g_tState to 'STATE_STARTING')
						//tStatus=CardPlayMp3File(g_PlayList.pCurrentEntry->szFileName);
						//if (tStatus==CARD_NO_SONG)
						//{
							//LogMsg_P(LOG_DEBUG, PSTR("skipping %s"), g_PlayList.pCurrentEntry->szFileName);
						//}
//
					//}while (tStatus!=OK);
				//}
			//}
		//}
		//else
		//{
			//if (tStatus!=OK)
			//{
				//LogMsg_P(LOG_ERR, PSTR("Not a valid Playlist"));
			//}
		//}
//
		//// clean up our sources...
		//_close(g_PlayList.fid);
		//g_PlayList.fid=0;
		//if (g_PlayList.pbyFileBuf)
		//{
			//MyFree(g_PlayList.pbyFileBuf);
		//}
//
		//g_tStatus = tStatus;
//
		//return(tStatus);
	//}

	///*!
	//* \brief open the playlist and validate. Read #songs as well
	//* \Note that 'byPlalist; starts at '0' while the filenames will start at '1'
	//*
	//*/
	//static TError CardOpenPlayList(u_char byPlayList)
	//{
		//char szFileName[MAX_FILENAMESIZE];
		//char szTag[12];
		//char* pszKeyString=NULL;
		//long lFileLength;
//
		///*
		//*  check if the list was already opened, if not, do so
		//*  and see if we are dealing with a valid playlist
		//*/
		//if (g_PlayList.fid==0)
		//{
			//// compose name to open
			//sprintf_P(szFileName, PSTR("FM0:%d.pls"), byPlayList+1);
			//LogMsg_P(LOG_INFO, PSTR("Opening: %s"), szFileName);
//
			//if ((g_PlayList.fid = _open(szFileName, _O_RDONLY)) == -1)
			//{
				//LogMsg_P(LOG_ERR, PSTR("Error opening: %s"), szFileName);
				//return(CARD_BADPLAYLIST);
			//}
			//else
			//{
				//lFileLength=_filelength(g_PlayList.fid);
				//if ((g_PlayList.pbyFileBuf=MyMalloc(lFileLength)) == NULL)
				//{
					//LogMsg_P(LOG_ERR, PSTR("Unable to allocate %ld bytes of RAM"), lFileLength);
					//return(CARD_NO_HEAP);
				//}
				//LogMsg_P(LOG_INFO, PSTR("Filesize: %ld bytes"), lFileLength);
//
				//// read entire file into our buffer
				//_read(g_PlayList.fid, g_PlayList.pbyFileBuf, lFileLength);
//
				//// introduce the global variable 'g_pbyFilePos' that navigates through RAM
				//g_pbyFilePos=g_PlayList.pbyFileBuf;
//
				//// Skip whitepace and empty lines
				//while (isspace(*g_pbyFilePos))
				//{
					//g_pbyFilePos++;
				//}
//
				//// try to find the [playlist] tag
				//strcpy_P(szTag, PSTR("[playlist]"));
//
				//if ((strncasecmp(g_pbyFilePos, szTag,  strlen(szTag))) != 0)
				//{
					//LogMsg_P(LOG_ERR, PSTR("%s tag not found"), szTag);
					//return(CARD_BADPLAYLIST);
				//}
//
				//g_pbyFilePos += strlen(szTag);
//
				//// try to find the 'NumberOfEntries' tag
				//strcpy_P(szTag, PSTR("NumberOfEntries"));
				//pszKeyString=CardReadPlayListTag(szTag);
				//if (pszKeyString!=NULL)
				//{
					//g_PlayList.uNrofSongs=atoi(pszKeyString);
					//MyFree(pszKeyString);
//
					//LogMsg_P(LOG_INFO, PSTR("Playlist reports %d songs"), g_PlayList.uNrofSongs);
				//}
				//else
				//{
					//return(CARD_BADPLAYLIST);
				//}
			//}
		//}
		//else
		//{
			//return(CARD_PLAYLIST_IN_USE);
		//}
		//return(OK);
	//}
//
	///*!
	//* \brief search for the tag in the data and return a string containing the value of the tag
	//*
	//*/
	//static char* CardReadPlayListTag(char* szTag)
	//{
		//char *pszKeyValue = NULL;
//
		//// skip whitespace (including newlines)
		//while ((*g_pbyFilePos == ' ')
		//|| (*g_pbyFilePos == '\t')
		//|| (*g_pbyFilePos == '\n')
		//|| (*g_pbyFilePos == '\r'))
		//{
			//g_pbyFilePos++;
		//}
//
//
		//if ((strncasecmp(g_pbyFilePos, szTag,  strlen(szTag))) != 0)
		//{
			////        LogMsg_P(LOG_ERR, PSTR("%s tag not found (found: %s"), szTag, g_pbyFilePos);
			//return(NULL);
		//}
//
		//g_pbyFilePos += strlen(szTag);
//
		//// now skip whitespace in front of the '=' symbol
		//while ((*g_pbyFilePos == ' ') || (*g_pbyFilePos == '\t'))
		//{
			//g_pbyFilePos++;
		//}
//
		//// did we get the '=' symbol?
		//if (*g_pbyFilePos != '=')
		//{
			//LogMsg_P(LOG_ERR, PSTR("could not find '=' symbol"));
			//return(NULL);
		//}
		//else
		//{
			//unsigned int unKeyLen = 0;
//
			//// skip '='
			//g_pbyFilePos++;
//
			//// now skip whitespace after the  '=' symbol
			//while ((*g_pbyFilePos == ' ') || (*g_pbyFilePos == '\t'))
			//{
				//g_pbyFilePos++;
			//}
//
			///* Find where the value ends */
			//while ((*(g_pbyFilePos + unKeyLen) != '\0') && (*(g_pbyFilePos + unKeyLen) != '\r') && (*(g_pbyFilePos + unKeyLen) != '\n'))
			//{
				//unKeyLen++;
			//}
			//// remove whitespace after the value but before the EOL
			//while ((unKeyLen > 0) && (isspace(*(g_pbyFilePos + unKeyLen - 1))))
			//{
				//unKeyLen--;
			//}
//
			//// create a NULL-terminated string from this value
			//if ((pszKeyValue = MyMalloc(unKeyLen + 1)) != NULL)
			//{
				//memcpy(pszKeyValue, g_pbyFilePos, unKeyLen);
				//pszKeyValue[unKeyLen] = '\0';
				//g_pbyFilePos += strlen(pszKeyValue);
			//}
			//else
			//{
				//return(NULL);       // unable to allocate RAM
			//}
		//}
//
		//return(pszKeyValue);
	//}
//
//
	///*!
	//* \brief read (next) song info from the playlist
	//*/
	//static TError CardGetNextSongFromPlayList(TPlayListEntry* pEntry)
	//{
		//char szTag[12];
		//char* pszKeyString=NULL;
//
		//if (pEntry==NULL)
		//{
			//return(CARD_NO_SONG);
		//}
//
		//// abuse for global counting now...
		//++g_PlayList.uCurrentSong;
//
		///*
		//*  try to find the 'File' tag,
		//*  If it doesn't exist, we must abort the routine
		//*/
		//sprintf_P(szTag, PSTR("File%d"), g_PlayList.uCurrentSong);
		//pszKeyString=CardReadPlayListTag(szTag);
		//if (pszKeyString!=NULL)
		//{
			//if (strlen(pszKeyString) > MAX_FILENAMESIZE)
			//{
				//pszKeyString[MAX_FILENAMESIZE]='\0';
			//}
			//strcpy(pEntry->szFileName, pszKeyString);
			//LogMsg_P(LOG_INFO, PSTR("Filename: %s"), pszKeyString);
			//MyFree(pszKeyString);
		//}
		//else
		//{
			//return(CARD_NO_SONG);
		//}
//
		///*
		//*  try to find the 'Title' tag,
		//*  If it doesn't exist, we just continue to search for the Length-tag
		//*/
		//sprintf_P(szTag, PSTR("Title%d"), g_PlayList.uCurrentSong);
		//pszKeyString=CardReadPlayListTag(szTag);
		//if (pszKeyString!=NULL)
		//{
			//if (strlen(pszKeyString) > DISPLAY_SIZE)
			//{
				//pszKeyString[DISPLAY_SIZE]='\0';
			//}
			//strcpy(pEntry->szTitle, pszKeyString);
			//LogMsg_P(LOG_INFO, PSTR("Title: %s"), pszKeyString);
			//MyFree(pszKeyString);
		//}
//
		///*
		//*  try to find the 'Length' tag,
		//*  Note that if it doesn't exist, we just return without an error
		//*/
		//sprintf_P(szTag, PSTR("Length%d"), g_PlayList.uCurrentSong);
		//pszKeyString=CardReadPlayListTag(szTag);
		//if (pszKeyString!=NULL)
		//{
			//pEntry->uLength=atoi(pszKeyString);
			//LogMsg_P(LOG_INFO, PSTR("Duration: %d"), atoi(pszKeyString));
			//MyFree(pszKeyString);
		//}
//
		//return(OK);
	//}


	/*!
	* \brief Get the name of the player status
	*
	*
	* \return pointer to string that contains the statename
	*/
	static char* CardGetState(TMMCState State)
	{
		switch ((int)State)
		{
			case STATE_IDLE             :return(strcpy_P(szCardState, PSTR("idle")));
			case STATE_STARTING         :return(strcpy_P(szCardState, PSTR("starting")));
			case STATE_PLAYING          :return(strcpy_P(szCardState, PSTR("playing")));
			case STATE_STOPPING         :return(strcpy_P(szCardState, PSTR("stopping")));

			default                     :return(strcpy_P(szCardState, PSTR("??")));
		}
	}

	/*!
	* \brief initialise this module
	*
	*/
	void CardInit()
	{
		char ThreadName[10];

		CardState=CARD_IDLE;
		CardPresentFlag=CARD_IS_NOT_PRESENT;

		g_tState = STATE_IDLE;

		/*
		* Create a main Card thread
		*/
		strcpy_P(ThreadName, PSTR("CardPlay"));

		if (GetThreadByName((char *)ThreadName) == NULL)
		{
			if (NutThreadCreate((char *)ThreadName, CardPlayer, 0, 768) == 0)
			{
				LogMsg_P(LOG_EMERG, PSTR("Thread failed"));
			}
		}

		/*
		* Create a CardPresent thread
		*/
		strcpy_P(ThreadName, PSTR("CardPres"));

		if (GetThreadByName((char *)ThreadName) == NULL)
		{
			if (NutThreadCreate((char *)ThreadName, CardPresent, 0, 768) == 0)
			{
				LogMsg_P(LOG_EMERG, PSTR("Thread failed"));
			}
		}

		/*
		*  report card (or not)
		*/
		if (bit_is_clear(MMC_IN_READ, MMC_CDETECT))
		{
			LogMsg_P(LOG_INFO, PSTR("Card present"));
		}
		else
		{
			LogMsg_P(LOG_INFO, PSTR("No Card present"));
		}


	}

	void MmcPlayFile()
	{
		if (g_NrofPlayLists == 1)
		{
			mystream = fopen("FM0:test1.mp3", "r" );
			LogMsg_P(LOG_INFO, PSTR("Found mp3. Creating play thread..."));
			play(mystream);
		}
		LogMsg_P(LOG_DEBUG, PSTR("play() is finished executing"));
	}

	//THREAD(MMCPlayer, arg)
	//{
		//NutThreadSetPriority(48);
		//
		//while(!CARD_NOT_PRESENT)
		//{
			//NutSleep(50);
		//}
		//
		//StopStream();
		//NutThreadDestroy();
		//NutThreadExit();
		//for(;;);
	//}

	/* ---------- end of module ------------------------------------------------ */

	/*@}*/


