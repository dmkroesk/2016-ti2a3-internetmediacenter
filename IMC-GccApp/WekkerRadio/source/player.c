
///
#define LOG_MODULE  LOG_MAIN_MODULE

#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "player.h"
#include "vs10xx.h"
#include "log.h"



#define OK			0
#define NOK			1

THREAD(StreamPlayer, arg);
static int playing = 0;

int initPlayer(void)
{
	return OK;
}

int play(FILE *stream)
{
	playing = 1;
	LogMsg_P(LOG_INFO, PSTR("Creating play thread..."));
	NutThreadCreate("Bg", StreamPlayer, stream, 512);
	printf("\nPlay thread created. Device is playing stream now !");
	
	return OK;
}

void stop() {
	VsPlayerStop();
	playing = 0;
}

THREAD(StreamPlayer, arg)
{
	NutThreadSetPriority(52);
	FILE *stream = (FILE *) arg;
	size_t rbytes = 0;
	char *mp3buf;
	int result = NOK;
	int nrBytesRead = 0;
	unsigned char iflag;
	
	//
	// Init MP3 buffer. NutSegBuf is een globale, systeem buffer
	//
	if( 0 != NutSegBufInit(12288) )
	{
		// Reset global buffer
		iflag = VsPlayerInterrupts(0);
		NutSegBufReset();
		VsPlayerInterrupts(iflag);
		
		result = OK;
	}
	LogMsg_P(LOG_INFO, PSTR("Initialized MP3 buffer (0 is OK), %d"), result);
	// Init the Vs1003b hardware
	if( OK == result )
	{
		LogMsg_P(LOG_DEBUG, PSTR("begin VsPlayerInit"));
		VsPlayerInit();
		LogMsg_P(LOG_DEBUG, PSTR("begin VsPlayerReset"));
		VsPlayerReset(VS_SM_RESET);
		//if( -1 == VsPlayerInit() )
		//{
			//if( -1 == VsPlayerReset(0) )
			//{
				//result = NOK;
			//}
		//}
	}
	LogMsg_P(LOG_INFO, PSTR("Initialized decoder (0 is OK), %d"), result);
	LogMsg_P(LOG_DEBUG, PSTR("let the stream play!"));
	while(playing == 1)
	{
		/*
		 * Query number of byte available in MP3 buffer.
		 */
        iflag = VsPlayerInterrupts(0);
        mp3buf = NutSegBufWriteRequest(&rbytes);
        VsPlayerInterrupts(iflag);
		
		// Bij de eerste keer: als player niet draait maak player wakker (kickit)
		if( VS_STATUS_RUNNING != VsGetStatus() )
		{
			if( rbytes < 1024 )
			{
				printf("\nVsPlayerKick()");
				VsPlayerKick();
			}
		}
		
		while( rbytes && playing == 1 )
		{
			// Copy rbytes (van 1 byte) van stream naar mp3buf.
			nrBytesRead = fread(mp3buf,1,rbytes,stream);
			
			if( nrBytesRead > 0 )
			{
				iflag = VsPlayerInterrupts(0);
				mp3buf = NutSegBufWriteCommit(nrBytesRead);
				VsPlayerInterrupts(iflag);
				if( nrBytesRead < rbytes && nrBytesRead < 512 )
				{
					LogMsg_P(LOG_DEBUG, PSTR("Nutsleep(250) bij if( nrBytesRead < rbytes && nrBytesRead < 512 )"));
					NutSleep(250);
				}
			}
			else
			{
				break;
			}
			rbytes -= nrBytesRead;
			
			if( nrBytesRead <= 0 )
			{
				LogMsg_P(LOG_DEBUG, PSTR("if( nrBytesRead <= 0 ) break;"));
				break;
				
			}				
		}
		
	}
	
	NutThreadExit();
	NutThreadDestroy();
	for(;;) {
		NutSleep(10);
	}
}

