/*
* FirstBootMenu.c
*
* Created: 26-2-2016 10:28:05
*  Author: Bart
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/timer.h>
#include <avr/eeprom.h>

#include "display.h"
#include "buttons.h"
#include "Menu/FirstBootMenu.h"

#include "settings.h"

static int utc;
static u_char finished;

static void ButtonUp( void );
static void ButtonDown( void );
static void ButtonOk( void );

void resetTimeZone() {
	SaveTimeZone('a');
}

void doFirstBoot() {
	finished = 0;
	// not used: str[10]
	//char str[10];
	writeLineOne("SIR100 starting");
	NutSleep(1000);
	ClearScreen();
	if(LoadTimeZone() >= -12 && LoadTimeZone() <= 12){
		finished =1;
		}else{
		writeLineOne("Set timezone");
		//utc = LoadTimeZone();
		showTimeZone(0);
	}
	while(finished == 0) {
		NutSleep(10);
	}
	buttonResetAll();
	NutSleep(100);
	//SaveAlarmTime(0, 0);
}

static void ButtonUp() {
	if (utc <= 11)
	utc++;

	showTimeZone(utc);
}

static void ButtonDown() {
	if (utc >= -11)
	utc--;

	showTimeZone(utc);
}

void showTimeZone(int utctime){
	char str[10];
	if(utc <= 0){
		sprintf(str, "UTC %d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
		}else{
		sprintf(str, "UTC +%d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
	}
}

static void ButtonOk() {
	char str[10];
	// send utc int back...
	printf("\nUTC: %d\n", utc);
	sprintf(str, "Saved UTC %d", utc);
	writeLineTwo(str);
	SaveTimeZone(utc);
	finished = 1;
}

void initFirstBoot() {
	
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctOK(&ButtonOk);
	utc = 0;
	finished = 0;
}

void SaveTimeZone(int _utc){
	SettingsSaveTimeZone(_utc);
}

int LoadTimeZone(){
	return SettingsLoadTimeZone();
}

