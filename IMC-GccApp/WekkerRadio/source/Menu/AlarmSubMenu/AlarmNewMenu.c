/*
* AlarmNewMenu.c
*
* Created: 2/25/2016 3:00:19 PM
*  Author: Test
*/
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"
#include "settings.h"

static int position = 1;

static char title[16] = "ALARM MENU";

static char menu1[16] = "Time";
static char menu2[16] = "Stream - Beep";

static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonUp( void );
static void ButtonDown( void );
static void ButtonBack( void );
static void ButtonOK( void );

static void ButtonLeft()
{
	if (SettingsGetAlarmTimeChangeBoolean() == 0)
	{
		position--;
		if(position == 0)
		{
			position = 2;
		}

		ClearScreen();
		ShowMenuNewAlarm();
	}
}

static void ButtonRight()
{
	if (SettingsGetAlarmTimeChangeBoolean() == 0)
	{
		position++;
		if(position == 3)
		{
			position = 1;
		}
		
		ClearScreen();
		ShowMenuNewAlarm();
	}
}

static void ButtonBack()
{
	SettingsAlarmTimeChangeBooleanFalse();
	ClearScreen();
	ShowMenuMain();
}

static void ButtonOK()
{
	if (SettingsGetAlarmTimeChangeBoolean() == 1)
	{
		SaveAlarmTime();
		SettingsAlarmTimeChangeBooleanFalse();
		ShowMenuNewAlarm();
	}
}

static void ButtonDown()
{
	int *beep;
	switch(position)
	{
		case 1:
			SettingsAlarmTimeChangeBooleanTrue();
			AlarmTimeDown();
			break;
		case 2:
			if (SettingsGetAlarmTimeChangeBoolean() == 0)
			{
				beep = GetBeep();
				if (*beep == 1)
				{
					*beep = 0;
				}
				else {
					*beep = 1;
				}
			}
			break;
	}
}

static void ButtonUp()
{
	int *beep;
	switch(position)
	{
		case 1:
			SettingsAlarmTimeChangeBooleanTrue();
			AlarmTimeUp();
		case 2:
			if (SettingsGetAlarmTimeChangeBoolean() == 0)
			{
				ClearScreen();
				beep = GetBeep();
				if(*beep == 1)
				{
					*beep = 0;
					writeLineTwo("Stream On");
				}
				else
				{
					*beep = 1;
					writeLineTwo("Beep On");
				}
			}
			break;
	}
}

void ShowMenuNewAlarm()
{
	buttonResetAll();
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctEsc(&ButtonBack);
	buttonSetFunctOK(&ButtonOK);
	writeLineOne(title);

	ShowAlarmTime();

	switch (position)
	{
		case 1:
			writeLineTwo(menu1);
			break;
		case 2:
			writeLineTwo(menu2);
			break;
	}
}


