/*
* SettingsMenu.c
*
* Created: 2/25/2016 3:00:56 PM
*  Author: Test
*/
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"

#include "settings.h"


static int position = 1;

static char title[16] = "SETTINGS MENU";

static char menu1[16] = "Timezone";
static char menu2[16] = "Snooze time";
static char menu3[16] = "Save settings";
static char menu4[16] = "Load settings";
static char menu5[16] = "Reset defaults";
static char menu6[16] = "Audio settings";


static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonUp( void );
static void ButtonDown( void );
static void ButtonOk( void );
static void ButtonBack( void );


static void ButtonLeft()
{
	SettingsSnoozeTimeChangeBooleanFalse();
	position--;
	if(position == 0)
	{
		position = 6;
	}

	ClearScreen();
	ShowMenuSettings();
}

static void ButtonRight()
{
	SettingsSnoozeTimeChangeBooleanFalse();
	position++;
	if(position == 7)
	{
		position = 1;
	}

	ClearScreen();
	ShowMenuSettings();
}

static void ButtonOk()
{
	if (SettingsGetSnoozeTimeChangeBoolean() == 0)
	{
		switch(position)
		{
			case 1:
				ClearScreen();
				ShowMenuTimeSetting();
				break;
			case 2:
				ClearScreen();
				ShowMenuSettings();
				break;
			case 3:
				writeLineTwo("Settings saved");
				SettingsSaveSettings();
				break;
			case 4:
				writeLineOne("Settings loaded");
				SettingsLoadSettings();
				break;
			case 5:
				ClearScreen();
				SetDefaultSettings();
				ShowMenuSettings();
				break;
			case 6:
			ClearScreen();
			ShowMenuAudio();
			break;
		}
	}
}

static void ButtonBack()
{
	SettingsSnoozeTimeChangeBooleanFalse();
	ClearScreen();
	ShowMenuMain();
}

static void ButtonDown()
{
	switch(position)
	{
		case 2:
			SettingsSnoozeTimeChangeBooleanTrue();
			SnoozeTimeDown();
			break;
	}
}

static void ButtonUp()
{
	switch(position)
	{
		case 2:
			SettingsSnoozeTimeChangeBooleanTrue();
			SnoozeTimeUp();
			break;
	}
}

void ShowMenuSettings()
{
	buttonResetAll();
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);
	writeLineOne(title);

	switch (position)
	{
		case 1:
			writeLineTwo(menu1);
			break;
		case 2:
			writeLineTwo(menu2);
			break;
		case 3:
			writeLineTwo(menu3);
			break;
		case 4:
			writeLineTwo(menu4);
			break;
		case 5:
			writeLineTwo(menu5);
			break;
		case 6:
			writeLineTwo(menu6);
			break;
	}
}
