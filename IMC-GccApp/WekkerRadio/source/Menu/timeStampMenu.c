/*
* timeStampMenu.c
*
* Created: 26-02-16 13:35:11
*  Author: Karel
*/

#define LOG_MODULE LOG_MAIN_MODULE

#include <stdio.h>

#include "display.h"
#include "buttons.h"
#include "settings.h"
#include "rtc.h"
#include "log.h"
#include <sys/timer.h>
#include <sys/thread.h>

#include "Menu/MainMenu.h"
#include "Menu/TimeStampMenu.h"
#include "Menu/firstbootmenu.h"

static int inMainMenu = 0;
static tm *datetimept;

static void gotoMainMenu( void );
static void ButtonUp( void );
static void ButtonDown( void );

THREAD(TimeLoopThread, args) {
	NutThreadSetPriority(46);
	for (;;) {
		if(inMainMenu == 0){
			char str[16];
			X12RtcGetClock(datetimept);
			sprintf( str , "%02d:%02d:%02d", datetimept->tm_hour + LoadTimeZone(), datetimept->tm_min, datetimept->tm_sec);
			writeLineTwo(str);
		}
		NutSleep(450);
	}
}

static void ButtonUp()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeUp();
}
static void ButtonDown()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeDown();
}

void SetInMainMenu() {
	inMainMenu = 1;
}

static void gotoMainMenu() 
{
	//volume wordt niet aangepast; laat hoofdmenu zien
	if (SettingsGetVolumeChangeBoolean() == 0)
	{
		LogMsg_P(LOG_INFO, PSTR("Go to main menu"));
		inMainMenu = 1;
		ClearScreen();
		ShowMenuMain();
	}
	
	//volume wordt aangepast; laat tijd weer zien
	if (SettingsGetVolumeChangeBoolean() == 1)
	{
		DisplayTimeStamp();
		SettingsVolumeChangeBooleanFalse();
	}
}

void InitTimeStampMenu(tm *_datetime) {
	datetimept = _datetime;
	DisplayTimeStamp();
}

void DisplayTimeStamp() {
	buttonResetAll();
	buttonSetFunctOK(&gotoMainMenu);
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	inMainMenu = 0;
	ClearScreen();
	writeLineOne("CURRENT TIME");
	NutThreadCreate("TimeLoopThread", TimeLoopThread, NULL, 256);
}

