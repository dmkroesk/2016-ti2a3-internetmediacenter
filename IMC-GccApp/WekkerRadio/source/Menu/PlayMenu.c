/*
* PlayMenu.c
*
* Created: 2/25/2016 2:50:48 PM
*  Author: Test
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"
#include "Menu/PlaySubMenu/SleepMenu.h"

static int position = 1;

static char title[16] = "PLAY MENU";

static char menu1[16] = "Playlists";
static char menu2[16] = "Streams";
static char menu3[16] = "Sleep";
static char menu4[16] = "SD Card";


static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonOk( void );
static void ButtonBack( void );


static void ButtonLeft()
{
	position--;
	if(position == 0)
	{
		position = 4;
	}

	ClearScreen();
	ShowMenuPlay();
}

static void ButtonRight()
{
	position++;
	if(position == 5)
	{
		position = 1;
	}

	ClearScreen();
	ShowMenuPlay();
}

static void ButtonOk()
{
	switch (position)
	{
		case 1:
			ClearScreen();
			ShowMenuPlaylist();
			break;
		case 2:
			ClearScreen();
			ShowMenuStream();
			break;
		case 3:
			ClearScreen();
			ShowSleepMenu();
			break;
		case 4:
			ClearScreen();
			ShowMenuSDCard();
			break;
	}
}

static void ButtonBack()
{
	ClearScreen();
	ShowMenuMain();
}

void ShowMenuPlay()
{
	buttonResetAll();
	writeLineOne(title);
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);

	switch (position)
	{
		case 1:
			writeLineTwo(menu1);
			break;
		case 2:
			writeLineTwo(menu2);
			break;
		case 3:
			writeLineTwo(menu3);
			break;
		case 4:
			writeLineTwo(menu4);
	}
}
