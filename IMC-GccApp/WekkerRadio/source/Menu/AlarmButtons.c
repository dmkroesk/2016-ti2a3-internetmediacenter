#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "Menu/MainMenu.h"
#include "alarm.h"
#include "settings.h"
#include "shoutcast.h"

#include "vs10xx.h"

#include <time.h>
#include "rtc.h"

#include "buttons.h"
#include "Menu/AlarmButtons.h"

#define LOG_MODULE  LOG_MAIN_MODULE
void	 (*oldButtons)(void);

static void ButtonBack( void );
static void ButtonOk( void );

// Our alarm time

static void ButtonBack(){
		int* getbeep = GetBeep();
		if(*getbeep == 1)
		{
			VsBeepStop();
		}
		else if(*getbeep == 0)
		{
			StopStream();
		}

    LogMsg_P(LOG_INFO, PSTR("Snoozing"));
    ResetAlarm();
    oldButtons();
}

static void ButtonOk(){
		int* getbeep = GetBeep();
		if(*getbeep == 1)
		{
			VsBeepStop();
		}
		else if(*getbeep == 0)
		{
			StopStream();
		}

	LogMsg_P(LOG_INFO, PSTR("Alarm disabled."));
	// Set old button handler back
	oldButtons();
}

void ActivateAlarmButtons(void (*bts)){
	buttonResetAll();
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);

	oldButtons = bts;
}

void DisplayCurrentAlarm() {
	writeLinesFirstLineInCapitals("alarm", "press OK to deactivate / Esc to Snooze");
}
