/*
* TimeSettingsMenu.c
*
* Created: 2/25/2016 3:00:56 PM
*  Author: Test
*/
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"
#include "Menu/FirstBootMenu.h"

static int utc;

static char title[16] = "TIME MENU";

static void ButtonOk( void );
static void ButtonBack( void );
static void ButtonUp( void );
static void ButtonDown( void );

static void ButtonOk()
{
	ClearScreen();
	SaveTimeZone(utc);
	ShowMenuTimeSetting();
}

static void ButtonBack()
{
	ClearScreen();
	ShowMenuSettings();
}

static void ButtonDown()
{
	ClearScreen();
	writeLineOne("TIMEZONE");
	utc--;
	char str[10];
	if(utc <= 0){
		sprintf(str, "UTC %d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
		}else{
		sprintf(str, "UTC +%d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
	}
}

static void ButtonUp()
{
	ClearScreen();
	writeLineOne("TIMEZONE");
	utc++;
	char str[10];
	if(utc <= 0){
		sprintf(str, "UTC %d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
		}else{
		sprintf(str, "UTC +%d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
	}
}

void ShowMenuTimeSetting()
{
	utc = LoadTimeZone();
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);
	writeLineOne(title);

	char str[10];
	if(utc <= 0){
		sprintf(str, "UTC %d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
		}else{
		sprintf(str, "UTC +%d", utc);
		printf("\n%s\n", str);
		writeLineTwo("             ");
		writeLineTwo(str);
	}
}
