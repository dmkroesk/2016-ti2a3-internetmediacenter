/*
* AudioSettingsMenu.c
*
* Created: 24-03-16 10:51:17
*  Author: Karel
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/SettingsSubMenu/AudioSettingsMenu.h"
#include "vs10xx.h"
#include "Menu/MainMenu.h"
#include "settings.h"

static char title[16] = "Audio Settings";

static char menu1[16] = "Bass";
static char menu2[16] = "Treble";

static int position = 1;

static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonUp( void );
static void ButtonDown( void );
static void ButtonOk( void );
static void ButtonBack( void );


static void ButtonLeft()
{
	position--;
	if(position == 0)
	{
		position = 2;
	}

	ClearScreen();
	ShowMenuAudio();
}

static void ButtonRight()
{
	position++;
	if(position == 3)
	{
		position = 1;
	}

	ClearScreen();
	ShowMenuAudio();
}

static void ButtonUp()
{
	switch (position)
	{
		case 1:
		BassUp();
		break;
		case 2:
		TrebleUp();
		break;
	}
}

static void ButtonDown()
{
	switch (position)
	{
		case 1:
		BassDown();
		break;
		case 2:
		TrebleDown();
		break;
	}
}
static void ButtonOk(){
	switch (position)
	{
		case 1:
		
		break;
		case 2:

		break;
	}
}

static void ButtonBack()
{
	ClearScreen();
	ShowMenuSettings();
}

void ShowMenuAudio()
{
	buttonResetAll();
	writeLineOne(title);
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);

	switch (position)
	{
		case 1:
		writeLineTwo(menu1);
		break;
		case 2:
		writeLineTwo(menu2);
		break;
	}
}

int VsSetBassReg(u_char bass)
{
	u_char ief;
	ief = VsPlayerInterrupts(0);
	VsRegWrite(VS_BASS_REG, bass);
	VsPlayerInterrupts(ief);
	return(0);
}

u_short VsGetBassReg()
{
	u_char ief;
	u_short vol;
	ief = VsPlayerInterrupts(0);
	vol=VsRegRead(VS_BASS_REG);
	VsPlayerInterrupts(ief);
	return(vol);
}


void TrebleUp(void){
	u_short treble = VsGetBassReg();
	if(treble<0xFFFF){
		treble+=0x1000;
		VsSetBassReg(treble);
	}
	writeLineTwo("Treble up");
}

void TrebleDown(void){
	u_short treble = VsGetBassReg();
	if(treble>0x0F00){
		treble-=0x1000;
		VsSetBassReg(treble);
	}
	writeLineTwo("Treble down");
}


void BassUp(void){
	u_short bass = VsGetBassReg();
	u_short temp = bass<<2;
	if(temp<0xFF00){
		bass+=0x0010;
		VsSetBassReg(bass);
	}
	writeLineTwo("Bass up");
}

void BassDown(void){
	u_short bass = VsGetBassReg();
	u_short temp = bass<<2;
	if(temp>0x0000){
		bass-=0x0010;
		VsSetBassReg(bass);
	}
	writeLineTwo("Bass down");
}
