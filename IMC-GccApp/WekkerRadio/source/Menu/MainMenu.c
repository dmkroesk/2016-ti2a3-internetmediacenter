/*
* MainMenu.c
*
* Created: 2/25/2016 11:54:17 AM
*  Author: Dani�l Compagner
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"
#include "buttons.h"
#include "display.h"
#include "Menu/TimeStampMenu.h"

static int position = 1;

static char title[16] = "MAIN MENU";

static char menu1[16] = "Play";
static char menu2[16] = "Schedule";
static char menu3[16] = "Alarm";
static char menu4[16] = "Settings";


static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonOk( void );
static void ButtonBack( void );



static void ButtonLeft()
{
	position--;
	if(position == 0)
	{
		position = 4;
	}

	ClearScreen();
	ShowMenuMain();
}

static void ButtonRight()
{
	position++;
	if(position == 5)
	{
		position = 1;
	}

	ClearScreen();
	ShowMenuMain();
}

static void ButtonOk()
{
	switch (position)
	{
		case 1:
			ClearScreen();
			ShowMenuPlay();
			break;
		case 2:
			ClearScreen();
			ShowMenuSchedule();
			break;
		case 3:
			ClearScreen();
			ShowMenuNewAlarm();
			break;
		case 4:
			ClearScreen();
			ShowMenuSettings();
			break;
	}
}

static void ButtonBack()
{
	//moet tijd weergeven
	DisplayTimeStamp();
}

void ShowMenuMain()
{
	buttonResetAll();
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);
	writeLineOne(title);

	switch (position)
	{
		case 1:
			writeLineTwo(menu1);
			break;
		case 2:
			writeLineTwo(menu2);
			break;
		case 3:
			writeLineTwo(menu3);
			break;
		case 4:
			writeLineTwo(menu4);
			break;
	}
}
