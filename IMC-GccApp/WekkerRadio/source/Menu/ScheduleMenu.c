/*
* Schedulee.c
*
* Created: 2/25/2016 2:56:01 PM
*  Author: Test
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"



static int position = 1;

static char title[16] = "SCHEDULE MENU";

static char menu1[16] = "New";
static char menu2[16] = "Edit";


static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonOk( void );
static void ButtonBack( void );


static void ButtonLeft()
{
	position--;
	if(position == 0)
	{
		position = 2;
	}

	ClearScreen();
	ShowMenuSchedule();
}

static void ButtonRight()
{
	position++;
	if(position == 3)
	{
		position = 1;
	}

	ClearScreen();
	ShowMenuSchedule();
}

static void ButtonOk()
{
	//Go too... position
}

static void ButtonBack()
{
	ClearScreen();
	ShowMenuMain();
}

void ShowMenuSchedule()
{
	buttonResetAll();
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);
	writeLineOne(title);

	switch (position)
	{
		case 1:
			writeLineTwo(menu1);
			break;
		case 2:
			writeLineTwo(menu2);
			break;
	}
}
