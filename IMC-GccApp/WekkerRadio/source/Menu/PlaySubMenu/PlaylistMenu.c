/*
* PlayMenu.c
*
* Created: 2/25/2016 2:50:48 PM
*  Author: Test
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"
#include "settings.h"


static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonUp( void );
static void ButtonDown( void );
static void ButtonOk( void );
static void ButtonBack( void );

static char title[16] = "PLAYLIST MENU";



void ButtonLeft()
{
}

void ButtonRight()
{
}

void ButtonUp()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeUp();
}

void ButtonDown()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeDown();
}

void ButtonOk()
{
	if (SettingsGetVolumeChangeBoolean() == 1)
	{
		SettingsVolumeChangeBooleanFalse();
		ClearScreen();
		ShowMenuPlaylist();
	}
}

void ButtonBack()
{
	if (SettingsGetVolumeChangeBoolean() == 0)
	{
		ClearScreen();
		ShowMenuPlay();
	}
}

void ShowMenuPlaylist()
{
	buttonResetAll();
	writeLineOne(title);
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);

	writeLineTwo("AC/DC - For Those About To Rock.mp3");
}
