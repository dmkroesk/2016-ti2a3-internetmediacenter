/*
* StreamMenu.c
*
* Created: 3/10/2016 12:21:26 PM
*  Author: Test
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"
#include "shoutcast.h"
#include "settings.h"

static char title[16] = "STREAM MENU";

static char menu1[16] = "Radio 1";
static char menu2[16] = "Radio 3FM";
static char menu3[16] = "70s J-pop";
static char menu4[16] = "Anime uk";

static int position = 1;
static int isOn = 0;

static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonUp( void );
static void ButtonDown( void );
static void ButtonOk( void );
static void ButtonBack( void );


static void ButtonLeft()
{
	position--;
	if(position == 0)
	{
		position = 4;
	}

	ClearScreen();
	ShowMenuStream();
}

static void ButtonRight()
{
	position++;
	if(position == 5)
	{
		position = 1;
	}

	ClearScreen();
	ShowMenuStream();
}

static void ButtonUp()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeUp();
}

static void ButtonDown()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeDown();
}

static void ButtonOk()
{
	if (SettingsGetVolumeChangeBoolean() == 1)
	{
		SettingsVolumeChangeBooleanFalse();
		ClearScreen();
		ShowMenuStream();
	}
	else
	{
		if (isOn == 0)
		{

			//Radio 1, ("145.58.52.144", 80, "/radio1-bb-mp3")
			//Radio 3 FM, ("145.58.52.144", 80, "/3fm-bb-mp3")
			//Radio 70 Japan, ("198.178.123.14", 7002, "/")
			//Radio Anime Japan, ("149.255.59.164", 8049, "/stream")
			switch (position)
			{
				case 1:
				ConnectToStream("145.58.52.144", 80, "/radio1-bb-mp3");
				break;
				case 2:
				ConnectToStream("145.58.52.144", 80, "/3fm-bb-mp3");
				break;
				case 3:
				ConnectToStream("198.178.123.14", 7002, "/");
				break;
				case 4:
				ConnectToStream("149.255.59.164", 8049, "/stream");
				break;
			}
			PlayStream();
			isOn = 1;
		}
		else
		{
			StopStream();
			isOn = 0;
			ButtonOk();
		}
	}
}

static void ButtonBack()
{
	if (SettingsGetVolumeChangeBoolean() == 0)
	{
		StopStream();
		NutSleep(250);
		ClearScreen();
		ShowMenuPlay();
	}
}

void ShowMenuStream()
{
	buttonResetAll();
	writeLineOne(title);
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);

	switch (position)
	{
		case 1:
		writeLineTwo(menu1);
		break;
		case 2:
		writeLineTwo(menu2);
		break;
		case 3:
		writeLineTwo(menu3);
		break;
		case 4:
		writeLineTwo(menu4);
		break;
	}
}
