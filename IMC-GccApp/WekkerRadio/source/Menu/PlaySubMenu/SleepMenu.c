#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "Menu/MainMenu.h"
#include "alarm.h"
#include "settings.h"
#include "time.h"
#include "timecalc.h"
#include "settings.h"
#include "shoutcast.h"
#include "sleeper.h"

#include "vs10xx.h"

#include <time.h>
#include "rtc.h"

#include "buttons.h"
#include "Menu/PlaySubMenu/SleepMenu.h"

#define LOG_MODULE  LOG_MAIN_MODULE

// Knoppen
static void ButtonEsc( void );
static void ButtonOk( void );
static void ButtonLeft( void );
static void ButtonRight( void );
static void ButtonUp( void );
static void ButtonDown( void );

static char title1[16] = "HOW LONG?";
static char title2[16] = "MIN";
static char title3[16] = "HRS";

static void ButtonEsc(){
	ShowMenuPlay();
}

static void ButtonOk(){
	if (periodInMinutes > 0){
		X12RtcGetClock(&setTime);
		addToClock(&setTime, 0, 0, 0, periodInMinutes / 60, periodInMinutes % 60, 0);
		SetSleepTimer();
	} else if (periodInMinutes == 0 && isRunning == TRUE){
		StopSleepTimer();
	}
}

static void ButtonLeft(){
	if (periodInMinutes - 1 == -1){
		// Do nothing
		return;
	} else {
		periodInMinutes--;
		PrintSleepTime();

	}
}

static void ButtonRight(){
	if (periodInMinutes + 1 > 240){ // Max 4 hours
		return;
	} else {
		periodInMinutes++;
		PrintSleepTime();

	}
}


static void ButtonUp() {     
	SettingsVolumeChangeBooleanTrue();
	VolumeUp(); 
}

static void ButtonDown()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeDown();
}

void PrintSleepTime(){
	char newString[18] = "";
	if (periodInMinutes / 60 > 0){
		sprintf(newString, "%i %s %i %s", 	
							periodInMinutes / 60, 
							title3, 
							periodInMinutes % 60,
							title2);
	} else {
		sprintf(newString, "%i %s", periodInMinutes , title2);
	}

	writeLineTwo(newString);
}


void ShowSleepMenu(){
	buttonResetAll();
	buttonSetFunctEsc(&ButtonEsc);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctLeft(&ButtonLeft);
	buttonSetFunctRight(&ButtonRight);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctUp(&ButtonUp);

	writeLineOne(title1);
}
