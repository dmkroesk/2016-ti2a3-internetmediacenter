/*
* StreamMenu.c
*
* Created: 3/10/2016 12:21:26 PM
*  Author: Bart
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>
#include "Menu/MainMenu.h"
#include "shoutcast.h"
#include "settings.h"
#include "mmc.h"

static char title[16] = "MP3 MENU";

static int playing = 0;

static void ButtonUp( void );
static void ButtonDown( void );
static void ButtonOk( void );
static void ButtonBack( void );

static void ButtonUp()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeUp();
}

static void ButtonDown()
{
	SettingsVolumeChangeBooleanTrue();
	VolumeDown();
}

static void ButtonOk()
{
	if (SettingsGetVolumeChangeBoolean() == 1)
	{
		SettingsVolumeChangeBooleanFalse();
		ClearScreen();
		ShowMenuSDCard();
	}
	else
	{
		
	}
}

static void ButtonBack()
{
	if (SettingsGetVolumeChangeBoolean() == 0)
	{
		StopStream();
		playing = 0;
		NutSleep(250);
		ClearScreen();
		ShowMenuPlay();
	}
}

void ShowMenuSDCard()
{
	buttonResetAll();
	writeLineOne(title);
	buttonSetFunctUp(&ButtonUp);
	buttonSetFunctDown(&ButtonDown);
	buttonSetFunctOK(&ButtonOk);
	buttonSetFunctEsc(&ButtonBack);
	if (playing == 0) {
		MmcPlayFile();
	}
	playing = 1;
	writeLineTwo("playing mp3");
}
