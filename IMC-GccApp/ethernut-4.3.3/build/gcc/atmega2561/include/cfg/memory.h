#ifndef _INCLUDE_CFG_MEMORY_H_
#define _INCLUDE_CFG_MEMORY_H_

/*
 * Do not edit! Automatically generated on Thu Mar 25 17:14:29 2010
 */

#ifndef NUTMEM_SIZE
#define NUTMEM_SIZE 8192
#endif

#ifndef NUTMEM_START
#define NUTMEM_START 0x200
#endif

#ifndef NUTMEM_RESERVED
#define NUTMEM_RESERVED 64
#endif

#ifndef NUTXMEM_SIZE
#define NUTXMEM_SIZE 24064
#endif

#ifndef NUTXMEM_START
#define NUTXMEM_START 0x2200
#endif

#ifndef NUTBANK_COUNT
#define NUTBANK_COUNT 30
#endif

#ifndef NUTBANK_START
#define NUTBANK_START 0x8000
#endif

#ifndef NUTBANK_SIZE
#define NUTBANK_SIZE 0x4000
#endif

#ifndef NUTBANK_SR
#define NUTBANK_SR 0xFFFF
#endif

#ifndef NUTBANK_OFFSET
#define NUTBANK_OFFSET 0x02
#endif

#ifndef MAX_AT45_DEVICES
#define MAX_AT45_DEVICES 1
#endif

#ifndef AT45_ERASE_WAIT
#define AT45_ERASE_WAIT 3000
#endif

#ifndef AT45_CHIP_ERASE_WAIT
#define AT45_CHIP_ERASE_WAIT 50000
#endif

#ifndef AT45_WRITE_POLLS
#define AT45_WRITE_POLLS 1000
#endif

#ifndef FLASH_CHIP_BASE
#define FLASH_CHIP_BASE 0x10000000
#endif

#ifndef FLASH_ERASE_WAIT
#define FLASH_ERASE_WAIT 3000
#endif

#ifndef FLASH_CHIP_ERASE_WAIT
#define FLASH_CHIP_ERASE_WAIT 50000
#endif

#ifndef FLASH_WRITE_POLLS
#define FLASH_WRITE_POLLS 1000
#endif

#ifndef FLASH_CONF_SECTOR
#define FLASH_CONF_SECTOR 0x6000
#endif


#endif
