#ifndef _INCLUDE_CFG_LCD_H_
#define _INCLUDE_CFG_LCD_H_

/*
 * Do not edit! Automatically generated on Thu Mar 25 17:14:29 2010
 */

#ifndef LCD_ROWS
#define LCD_ROWS 2
#endif

#ifndef LCD_COLS
#define LCD_COLS 16
#endif

#ifndef LCD_SHORT_DELAY
#define LCD_SHORT_DELAY 10
#endif

#ifndef LCD_LONG_DELAY
#define LCD_LONG_DELAY 1000
#endif


#endif
