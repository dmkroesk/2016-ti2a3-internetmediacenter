#ifndef _INCLUDE_CFG_ARCH_AVRPIO_H_
#define _INCLUDE_CFG_ARCH_AVRPIO_H_

/*
 * Do not edit! Automatically generated on Thu Mar 25 17:14:29 2010
 */

#ifndef LANC111_SIGNAL_IRQ
#define LANC111_SIGNAL_IRQ INT5
#endif

#ifndef RTL_BASE_ADDR
#define RTL_BASE_ADDR 0xC300
#endif

#ifndef RTL_SIGNAL_IRQ
#define RTL_SIGNAL_IRQ INT5
#endif

#ifndef RTL_EESK_BIT
#define RTL_EESK_BIT 5
#endif

#ifndef RTL_EESK_AVRPORT
#define RTL_EESK_AVRPORT AVRPORTC
#endif

#ifndef RTL_EEDO_BIT
#define RTL_EEDO_BIT 6
#endif

#ifndef RTL_EEDO_AVRPORT
#define RTL_EEDO_AVRPORT AVRPORTC
#endif

#ifndef RTL_EEMU_BIT
#define RTL_EEMU_BIT 7
#endif

#ifndef RTL_EEMU_AVRPORT
#define RTL_EEMU_AVRPORT AVRPORTC
#endif

#ifndef VS10XX_SIGNAL_IRQ
#define VS10XX_SIGNAL_IRQ INT6
#endif


#endif
