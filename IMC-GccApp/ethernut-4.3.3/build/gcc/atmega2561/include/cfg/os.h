#ifndef _INCLUDE_CFG_OS_H_
#define _INCLUDE_CFG_OS_H_

/*
 * Do not edit! Automatically generated on Thu Mar 25 17:14:29 2010
 */

#ifndef NUT_THREAD_IDLESTACK
#define NUT_THREAD_IDLESTACK 384
#endif

#ifndef NUT_THREAD_MAINSTACK
#define NUT_THREAD_MAINSTACK 768
#endif

#ifndef NUT_CPU_FREQ
#define NUT_CPU_FREQ 14745600
#endif


#endif
