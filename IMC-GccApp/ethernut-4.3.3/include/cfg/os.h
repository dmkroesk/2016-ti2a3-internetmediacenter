#ifndef _INCLUDE_CFG_OS_H_
#define _INCLUDE_CFG_OS_H_

/*
 * Do not edit! Automatically generated on Thu Feb 04 12:57:13 2016
 */

#ifndef NUT_THREAD_IDLESTACK
#define NUT_THREAD_IDLESTACK 384
#endif

#ifndef NUT_THREAD_MAINSTACK
#define NUT_THREAD_MAINSTACK 768
#endif

#ifndef NUT_CPU_FREQ
#define NUT_CPU_FREQ 14745600
#endif


#endif
