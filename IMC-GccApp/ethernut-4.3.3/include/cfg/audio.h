#ifndef _INCLUDE_CFG_AUDIO_H_
#define _INCLUDE_CFG_AUDIO_H_

/*
 * Do not edit! Automatically generated on Thu Feb 04 12:57:13 2016
 */

#ifndef VS10XX_FREQ
#define VS10XX_FREQ 12288000
#endif

#ifndef VS10XX_SCI_MODE
#define VS10XX_SCI_MODE 0
#endif

#ifndef VS10XX_SDI_MODE
#define VS10XX_SDI_MODE 0
#endif

#ifndef VS10XX_HWRST_DURATION
#define VS10XX_HWRST_DURATION 1
#endif

#ifndef VS10XX_HWRST_RECOVER
#define VS10XX_HWRST_RECOVER 4
#endif

#ifndef VS10XX_SWRST_RECOVER
#define VS10XX_SWRST_RECOVER 2
#endif


#endif
